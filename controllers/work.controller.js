const models = require("../models/init-models");

exports.find = (req, res) => {
  models.tb_p_pengalaman_kerja
    .findAll({
      where: {
        nik: req.user.nik,
      },
      include: [
        {
          model: models.tb_p_pengalaman_kerja_role,
          as: "tb_p_pengalaman_kerja_roles",
          include: [
            {
              model: models.tb_m_daftar_pekerjaan,
              as: "kd_jabatan_tb_m_daftar_pekerjaan",
            },
          ],
        },
      ],
    })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message ||
          "Error retrieving work experience data with nik: " + req.user.nik,
      });
    });
};

exports.findOne = (req, res) => {
  models.tb_p_pengalaman_kerja
    .findOne({
      where: {
        id_pengalaman_kerja: req.params.id_pengalaman_kerja,
      },
      include: [
        {
          model: models.tb_p_pengalaman_kerja_role,
          as: "tb_p_pengalaman_kerja_roles",
          include: [
            {
              model: models.tb_m_daftar_pekerjaan,
              as: "kd_jabatan_tb_m_daftar_pekerjaan",
            },
          ],
        },
      ],
    })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message ||
          "Error retrieving work experience data with id: " +
            req.params.id_pengalaman_kerja,
      });
    });
};

exports.create = (req, res) => {
  // Validate request
  if (Object.keys(req.body).length === 0) {
    res.status(400).send({
      message: "Content can not be empty!",
    });
  }

  const work = {
    nik: req.user.nik,
    perusahaan: req.body.perusahaan,
    tanggal_masuk: req.body.tanggal_masuk,
    tanggal_keluar: req.body.tanggal_keluar,
    status_pegawai: req.body.status_pegawai,
  };

  models.tb_p_pengalaman_kerja
    .create(work)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message ||
          "Some error occurred while creating the working experience.",
      });
    });
};

exports.update = (req, res) => {
  // Validate Request
  if (Object.keys(req.body).length === 0) {
    res.status(400).send({
      message: "Content can not be empty!",
    });
  }

  const work = {
    nik: req.user.nik,
    perusahaan: req.body.perusahaan,
    tanggal_masuk: req.body.tanggal_masuk,
    tanggal_keluar: req.body.tanggal_keluar,
    status_pegawai: req.body.status_pegawai,
  };

  models.tb_p_pengalaman_kerja
    .update(work, {
      where: {
        id_pengalaman_kerja: req.params.id_pengalaman_kerja,
      },
    })
    .then((num) => {
      if (num == 1) {
        res.send({ message: "Work experienece was update successfully!" });
      } else {
        res.send({
          message: `Cannot update work exp with id_pengalaman_kerja: ${req.params.id_pengalaman_kerja}. Maybe work exp was not found!`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message ||
          "Error updating working experience with id: " +
            req.params.id_pengalaman_kerja,
      });
    });
};

exports.delete = (req, res) => {
  models.tb_p_pengalaman_kerja
    .destroy({
      where: {
        id_pengalaman_kerja: req.params.id_pengalaman_kerja,
      },
    })
    .then((num) => {
      if (num == 1) {
        res.send({ message: `Work was deleted successfully!` });
      } else {
        res.send({
          message: `Cannot delete work exp with id_pengalaman_kerja: ${req.params.id_pengalaman_kerja}. Maybe work exp was not found!`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message ||
          "Error deleteing working experience with id: " +
            req.params.id_pengalaman_kerja,
      });
    });
};

exports.findRole = (req, res) => {
  models.tb_p_pengalaman_kerja_role
    .findAll({
      where: {
        id_pengalaman_kerja: req.params.id_pengalaman_kerja,
      },
      include: [
        {
          model: models.tb_m_daftar_pekerjaan,
          as: "kd_jabatan_tb_m_daftar_pekerjaan",
        },
      ],
    })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          "Error retrieving work experience role data Person with id= " +
          req.params.id_pengalaman_kerja,
      });
    });
};

exports.findOneRole = (req, res) => {
  models.tb_p_pengalaman_kerja_role
    .findByPk(req.params.id_role)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          "Error retrieving work experience role data with id= " +
          req.params.id_role,
      });
    });
};

exports.createRole = (req, res) => {
  // Validate request
  if (Object.keys(req.body).length === 0) {
    res.status(400).send({
      message: "Content can not be empty!",
    });
  }

  const wr = {
    id_pengalaman_kerja: req.params.id_pengalaman_kerja,
    kd_jabatan: req.body.kd_jabatan,
  };

  models.tb_p_pengalaman_kerja_role
    .create(wr)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the working role.",
      });
    });
};

exports.deleteRole = (req, res) => {
  models.tb_p_pengalaman_kerja_role
    .destroy({
      where: {
        id_role: req.params.id_role,
      },
    })
    .then((num) => {
      if (num == 1) {
        res.send({ message: `Work role was deleted successfully!` });
      } else {
        res.send({
          message: `Cannot delete work role with id_role: ${req.params.id_role}. Maybe work role was not found!`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message ||
          "Could not delete work role with id " + req.params.id_role,
      });
    });
};
