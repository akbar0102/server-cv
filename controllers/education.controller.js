const models = require("../models/init-models");

exports.find = (req, res) => {
  models.tb_p_pendidikan
    .findAll({
      where: {
        nik: req.user.nik,
      },
      include: [
        { model: models.tb_m_gelar, as: "tingkat_pendidikan_tb_m_gelar" },
      ],
    })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message ||
          "Error retrieving education data with nik: " + req.user.nik,
      });
    });
};

exports.findOne = (req, res) => {
  models.tb_p_pendidikan
    .findOne({
      where: {
        id_pendidikan: req.params.id_pendidikan,
      },
      include: [
        { model: models.tb_m_gelar, as: "tingkat_pendidikan_tb_m_gelar" },
      ],
    })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message ||
          "Error retrieving education data with id: " +
            req.params.id_pendidikan,
      });
    });
};

exports.create = (req, res) => {
  // Validate request
  if (Object.keys(req.body).length === 0) {
    res.status(400).send({
      message: "Content can not be empty!",
    });
  }

  const edu = {
    nik: req.user.nik,
    instansi: req.body.instansi,
    tingkat_pendidikan: req.body.tingkat_pendidikan,
    jurusan: req.body.jurusan,
    tahun_masuk: req.body.tahun_masuk,
    tahun_keluar: req.body.tahun_keluar,
  };

  models.tb_p_pendidikan
    .create(edu)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the education.",
      });
    });
};

exports.update = (req, res) => {
  // Validate Request
  if (Object.keys(req.body).length === 0) {
    res.status(400).send({
      message: "Content can not be empty!",
    });
  }

  const education = {
    nik: req.user.nik,
    instansi: req.body.instansi,
    tingkat_pendidikan: req.body.tingkat_pendidikan,
    jurusan: req.body.jurusan,
    tahun_masuk: req.body.tahun_masuk,
    tahun_keluar: req.body.tahun_keluar,
  };

  models.tb_p_pendidikan
    .update(education, {
      where: {
        id_pendidikan: req.params.id_pendidikan,
      },
    })
    .then((num) => {
      if (num == 1) {
        res.send({ message: "Education was update successfully!" });
      } else {
        res.send({
          message: `Cannot update education with id=${req.params.id_pendidikan}. Maybe education was not found!`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message ||
          "Error updating education with id: " + req.params.id_pendidikan,
      });
    });
};

exports.delete = (req, res) => {
  models.tb_p_pendidikan
    .destroy({
      where: {
        id_pendidikan: req.params.id_pendidikan,
      },
    })
    .then((num) => {
      if (num == 1) {
        res.send({ message: `Education was deleted successfully!` });
      } else {
        res.send({
          message: `Cannot delete education with id=${req.params.id_pendidikan}. Maybe education was not found!`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message ||
          "Error deleting education with id: " + req.params.id_pendidikan,
      });
    });
};
