const models = require("../models/init-models");

exports.find = (req, res) => {
  models.tb_project
    .findAll({
      where: {
        nik: req.user.nik,
      },
      include: [
        {
          model: models.tb_project_role,
          as: "tb_project_roles",
          include: [
            {
              model: models.tb_m_daftar_pekerjaan,
              as: "role_tb_m_daftar_pekerjaan",
            },
          ],
        },
      ],
    })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message ||
          "Error retrieving project data with nik: " + req.user.nik,
      });
    });
};

exports.findOne = (req, res) => {
  models.tb_project
    .findOne({
      where: {
        id_project: req.params.id_project,
      },
      include: [
        {
          model: models.tb_project_role,
          as: "tb_project_roles",
          include: [
            {
              model: models.tb_m_daftar_pekerjaan,
              as: "role_tb_m_daftar_pekerjaan",
            },
          ],
        },
      ],
    })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message ||
          "Error retrieving project data with id: " + req.params.id_project,
      });
    });
};

exports.create = (req, res) => {
  // Validate request
  if (Object.keys(req.body).length === 0) {
    res.status(400).send({
      message: "Content can not be empty!",
    });
  }

  const project = {
    nik: req.user.nik,
    nama_project: req.body.nama_project,
    tanggal_mulai: req.body.tanggal_mulai,
    tanggal_selesai: req.body.tanggal_selesai,
    customer: req.body.customer,
    end_customer: req.body.end_customer,
    project_site: req.body.project_site,
    deskripsi_project: req.body.deskripsi_project,
    tech_info: req.body.tech_info,
    other_info: req.body.other_info,
    project_lang: req.body.project_lang,
    project_framework: req.body.project_framework,
    project_app: req.body.project_app,
    project_tool: req.body.project_tool,
    project_db: req.body.project_db,
    project_os: req.body.project_os,
    project_server: req.body.project_server,
  };

  models.tb_project
    .create(project)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the project.",
      });
    });
};

exports.update = (req, res) => {
  // Validate request
  if (Object.keys(req.body).length === 0) {
    res.status(400).send({
      message: "Content can not be empty!",
    });
  }

  const project = {
    nik: req.user.nik,
    nama_project: req.body.nama_project,
    tanggal_mulai: req.body.tanggal_mulai,
    tanggal_selesai: req.body.tanggal_selesai,
    customer: req.body.customer,
    end_customer: req.body.end_customer,
    project_site: req.body.project_site,
    deskripsi_project: req.body.deskripsi_project,
    tech_info: req.body.tech_info,
    other_info: req.body.other_info,
    project_lang: req.body.project_lang,
    project_framework: req.body.project_framework,
    project_app: req.body.project_app,
    project_tool: req.body.project_tool,
    project_db: req.body.project_db,
    project_os: req.body.project_os,
    project_server: req.body.project_server,
  };

  models.tb_project
    .update(project, {
      where: {
        id_project: req.params.id_project,
      },
    })
    .then((num) => {
      if (num == 1) {
        res.send({ message: "Project was update successfully!" });
      } else {
        res.send({
          message: `Cannot update project with id_project: ${req.params.id_project}. Maybe project was not found!`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message ||
          "Error updating project with id: " + req.params.id_project,
      });
    });
};

exports.delete = (req, res) => {
  models.tb_project
    .destroy({
      where: {
        id_project: req.params.id_project,
      },
    })
    .then((num) => {
      if (num == 1) {
        res.send({ message: `Project was deleted successfully!` });
      } else {
        res.send({
          message: `Cannot delete project with id=${req.params.id_project}. Maybe project was not found!`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message ||
          "Error deleting project with id: " + req.params.id_project,
      });
    });
};

exports.findRole = (req, res) => {
  models.tb_project_role
    .findAll({
      where: {
        id_project: req.params.id_project,
      },
      include: [
        {
          model: models.tb_m_daftar_pekerjaan,
          as: "role_tb_m_daftar_pekerjaan",
        },
      ],
    })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message ||
          "Error retrieving project role data Person with id= " +
            req.params.id_project,
      });
    });
};

exports.findOneRole = (req, res) => {
  models.tb_project_role
    .findByPk(req.params.id_pr)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message ||
          "Error retrieving project role data with id= " + req.params.id_pr,
      });
    });
};

exports.createRole = (req, res) => {
  // Validate request
  if (Object.keys(req.body).length === 0) {
    res.status(400).send({
      message: "Content can not be empty!",
    });
  }

  const pr = {
    id_project: req.params.id_project,
    role: req.body.role,
  };

  models.tb_project_role
    .create(pr)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the project role.",
      });
    });
};

exports.deleteRole = (req, res) => {
  models.tb_project_role
    .destroy({
      where: {
        id_pr: req.params.id_pr,
      },
    })
    .then((num) => {
      if (num == 1) {
        res.send({ message: `Project role was deleted successfully!` });
      } else {
        res.send({
          message: `Cannot delete project role with id=${req.params.id_pr}. Maybe project role was not found!`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message ||
          "Could not delete project role with id " + req.params.id_pr,
      });
    });
};
