const models = require("../models/init-models");

exports.findAll = (req, res) => {
  models.tb_m_daftar_pekerjaan
    .findAll()
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Error retrieving all list role data",
      });
    });
};

exports.create = (req, res) => {
  // Validate request
  if (Object.keys(req.body).length === 0) {
    res.status(400).send({
      message: "Content can not be empty!",
    });
  }

  const job = {
    kd_jabatan: req.body.kd_jabatan,
    nama_jabatan: req.body.nama_jabatan,
  };

  models.tb_m_daftar_pekerjaan
    .create(job)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occurred while creating the job.",
      });
    });
};

exports.findOne = (req, res) => {
  models.tb_m_daftar_pekerjaan
    .findOne({
      where: {
        kd_jabatan: req.params.kd_jabatan,
      },
    })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message ||
          "Error retrieving job with id " + req.params.kd_jabatan,
      });
    });
};

exports.update = (req, res) => {
  if (Object.keys(req.body).length === 0) {
    res.status(400).send({
      message: "Content can not be empty!",
    });
  }

  const job = {
    nama_jabatan: req.body.nama_jabatan,
  };

  models.tb_m_daftar_pekerjaan
    .update(job, {
      where: {
        kd_jabatan: req.params.kd_jabatan,
      },
    })
    .then((num) => {
      if (num == 1) {
        res.send({ message: "Job was update successfully!" });
      } else {
        res.send({
          message: `Cannot update job with kd_jabatan: ${req.params.kd_jabatan}. Maybe job role was not found!`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message ||
          "Could not update job with id: " + req.params.kd_jabatan,
      });
    });
};
