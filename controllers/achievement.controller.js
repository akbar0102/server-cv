const models = require("../models/init-models");

exports.find = async (req, res) => {
  try {
    let prestasi = await models.tb_p_prestasi.findAll({
      where: {
        nik: req.user.nik,
      },
      include: [
        {
          model: models.tb_m_daftar_pekerjaan,
          as: "kd_jabatan_tb_m_daftar_pekerjaan",
        },
      ],
    });

    res.send(prestasi);
  } catch (error) {
    res.status(500).send({
      message:
        error.message ||
        "Error retrieving achievement data with nik: " + req.user.nik,
    });
  }
};

exports.findOne = (req, res) => {
  models.tb_p_prestasi
    .findOne({
      where: {
        id_prestasi: req.params.id_prestasi,
      },
      include: [
        {
          model: models.tb_m_daftar_pekerjaan,
          as: "kd_jabatan_tb_m_daftar_pekerjaan",
        },
      ],
    })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message ||
          "Error retrieving achievement data with id: " +
            req.params.id_prestasi,
      });
    });
};

exports.create = (req, res) => {
  // Validate request
  if (Object.keys(req.body).length === 0) {
    res.status(400).send({
      message: "Content can not be empty!",
    });
  }

  const achievement = {
    nik: req.user.nik,
    kd_jabatan: req.body.kd_jabatan,
    detail: req.body.detail,
  };

  models.tb_p_prestasi
    .create(achievement)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the achievement.",
      });
    });
};

exports.update = (req, res) => {
  // Validate Request
  if (Object.keys(req.body).length === 0) {
    res.status(400).send({
      message: "Content can not be empty!",
    });
  }

  const achievement = {
    nik: req.user.nik,
    kd_jabatan: req.body.kd_jabatan,
    detail: req.body.detail,
  };

  models.tb_p_prestasi
    .update(achievement, {
      where: {
        id_prestasi: req.params.id_prestasi,
      },
    })
    .then((data) => {
      res.status(200).json(data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          "Error updating achievement with id: " + req.params.id_prestasi,
      });
    });
};

exports.delete = (req, res) => {
  models.tb_p_prestasi
    .destroy({
      where: {
        id_prestasi: req.params.id_prestasi,
      },
    })
    .then((data) => {
      res.send({ message: `Achievement was deleted successfully!` });
    })
    .catch((err) => {
      res.status(500).send({
        message:
          "Error deleting achievement with id: " + req.params.id_prestasi,
      });
    });
};
