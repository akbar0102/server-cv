const wordTemplate = require('../common/docxTemplate');
const models = require("../models/init-models");
const fs = require('fs-extra');
const path = require('path');

exports.findALl = (req, res) => {
  models.tb_personal.findAll().then(data=>{
    res.send(data);
  }).catch(err => {
    res.status(500).send({
      message: err.message || "Some error occured while retrieving data all personal"
    })
  });

}

exports.findOne = (req, res) => {
  models.tb_personal.findOne({
    where: {
      nik: req.user.nik
    },
    include: [
      {model: models.tb_p_pengalaman_bahasa, as: 'tb_p_pengalaman_bahasas'},
      {model: models.tb_m_daftar_pekerjaan, as: "role_terakhir_tb_m_daftar_pekerjaan"},
      {model: models.tb_m_agama, as: "agama_tb_m_agama"}
    ]
  }).then(data => {
    res.send(data);
  }).catch(err => {
    res.status(500).send({
      message: err.message || "Error retrieving personal data with nik: " + req.user.nik
    });
  });
};

exports.update = async (req, res) => {
  try {
    let personal = {};
    // Validate request
    if (!req.body) {
      res.status(400).send({
        message: "Content can not be empty!",
      });
    }

    const person = await models.tb_personal.findOne({
      where: {
        nik: req.user.nik
      }
    });

    if(req.file == undefined){
      personal = {
        nama_depan: req.body.nama_depan,
        nama_belakang: req.body.nama_belakang,
        tempat_lahir: req.body.tempat_lahir,
        tanggal_lahir: req.body.tanggal_lahir,
        agama: req.body.agama,
        jenis_kelamin: req.body.jenis_kelamin,
        kesehatan: req.body.kesehatan,
        role_terakhir: req.body.role_terakhir,
        list_profile: req.body.list_profile,
        list_ptech: req.body.list_ptech
      };
    }else{
      if(person.dataValues.foto != null) {
        await fs.unlink(path.join(`public/${person.dataValues.foto}`))
      }

      personal = {
        nama_depan: req.body.nama_depan,
        nama_belakang: req.body.nama_belakang,
        tempat_lahir: req.body.tempat_lahir,
        tanggal_lahir: req.body.tanggal_lahir,
        agama: req.body.agama,
        jenis_kelamin: req.body.jenis_kelamin,
        kesehatan: req.body.kesehatan,
        foto: `images/${req.file.filename}`,
        role_terakhir: req.body.role_terakhir,
        list_profile: req.body.list_profile,
        list_ptech: req.body.list_ptech
      };
    }

    models.tb_personal.update(personal, {
      where: {
        nik: req.user.nik
      }
    }).then(data => {
      res.status(200).json(data);
    }).catch(err => {
      res.status(500).send({
        message: "Error updating personal with nik: " + req.user.nik
      });
    });
  } catch (error) {
    res.status(500).send({
      message: error
    });
  }
};

exports.createBahasa = (req, res) => {
  // Validate request
  if (!req.body) {
    res.status(400).send({
      message: "Content can not be empty!",
    });
  }

  const bahasa = {
    nik: req.user.nik,
    bahasa: req.body.bahasa,
    tingkat: req.body.tingkat,
  };

  models.tb_p_pengalaman_bahasa.create(bahasa)
    .then(data => {
      res.send(data);
    }).catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the experience language."
      });
    });
};

exports.readBahasa = (req, res) =>{
  models.tb_p_pengalaman_bahasa.findAll({
    where: {
      nik: req.user.nik
    }
  }).then(data => {
    res.send(data);
  }).catch(err => {
    res.status(500).send({
      message:
        err.message || "Some error occurred while retrieving person language experience list."
    });
  })
}

exports.readOneBahasa = (req, res) =>{
  models.tb_p_pengalaman_bahasa.findByPk(req.params.id_bahasa)
    .then(data => {
      res.send(data);
    }).catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving language experience."
      });
    })
}

exports.updateBahasa = (req, res) => {
  if (!req.body) {
    res.status(400).send({
      message: "Content can not be empty!",
    });
  }

  const lang = {
   nik: req.user.nik,
   bahasa: req.body.bahasa,
   tingkat: req.body.tingkat
  };

  models.tb_p_pengalaman_bahasa.update(lang, {
    where: {
      id_bahasa: req.params.id_bahasa
    }
  }).then(data => {
    res.send({ message: `Language exp was updated successfully!` });
  }).catch(err => {
    res.status(500).send({
      message: "Error updating language experience with id= " + req.params.id_bahasa
    });
  })
}

exports.deleteBahasa = (req, res) => {
  models.tb_p_pengalaman_bahasa.destroy({
    where: {
      id_bahasa: req.params.id_bahasa
    }
  }).then(data => {
    res.send({ message: `Language exp was deleted successfully!` });
  }).catch(err => {
    res.status(500).send({
      message: "Could not delete language experience with id= " + req.params.id_bahasa
    });
  });
}


exports.generateDocx = async (req, res) =>{
  let data = {};

  const personal = await models.tb_personal.findOne({ 
    where: {
      nik: req.user.nik
    } ,
    include: [
      {model: models.tb_p_pengalaman_bahasa, as: 'tb_p_pengalaman_bahasas'},
      {model: models.tb_m_daftar_pekerjaan, as: "role_terakhir_tb_m_daftar_pekerjaan"},
      {model: models.tb_m_agama, as: "agama_tb_m_agama"}
    ]
  });

  const education = await models.tb_p_pendidikan.findAll({
    where:{
      nik: req.user.nik,
    },
    include: [
      {model: models.tb_m_gelar, as: "tingkat_pendidikan_tb_m_gelar"}
    ]
  });

  const work = await models.tb_p_pengalaman_kerja.findAll({
    where: {
      nik: req.user.nik,
    },
    include: [{
      model: models.tb_p_pengalaman_kerja_role, as: "tb_p_pengalaman_kerja_roles", 
      include: [{
        model: models.tb_m_daftar_pekerjaan, as: "kd_jabatan_tb_m_daftar_pekerjaan"
        }
      ]}
    ]
  });

  const course = await models.tb_p_kursus.findAll({
    where: {
      nik: req.user.nik
    },
    include: [
      {model: models.tb_m_daftar_pekerjaan, as: "role_tb_m_daftar_pekerjaan"}
    ]
  });

  const project = await models.tb_project.findAll({
    where: {
      nik: req.user.nik
    },
    include: [
      {model: models.tb_project_role, as: "tb_project_roles", include: [
        {model: models.tb_m_daftar_pekerjaan, as: "role_tb_m_daftar_pekerjaan"}
      ]}
    ]
  });

  const accomplishment = await models.tb_p_prestasi.findAll({
    where: {
      nik: req.user.nik
    },
    include: [
      {model: models.tb_m_daftar_pekerjaan, as: "kd_jabatan_tb_m_daftar_pekerjaan"}
    ]
  });


  data.personal = personal;
  data.education = education;
  data.course = course;
  data.work = work;
  data.project = project;
  data.accomplishment = accomplishment;

  wordTemplate(res, data).then((response) => {
    res.send(response);
  }).catch((err) => {
    console.log(err);
  })

}
