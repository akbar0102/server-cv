const models = require("../models/init-models");

exports.find = (req, res) => {
  models.tb_p_kursus
    .findAll({
      where: {
        nik: req.user.nik,
      },
      include: [
        {
          model: models.tb_m_daftar_pekerjaan,
          as: "role_tb_m_daftar_pekerjaan",
        },
      ],
    })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message ||
          "Error retrieving course data with nik: " + req.user.nik,
      });
    });
};

exports.findOne = (req, res) => {
  models.tb_p_kursus
    .findOne({
      where: {
        id_kursus: req.params.id_kursus,
      },
      include: [
        {
          model: models.tb_m_daftar_pekerjaan,
          as: "role_tb_m_daftar_pekerjaan",
        },
      ],
    })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message ||
          "Error retrieving course data with id: " + req.params.id_kursus,
      });
    });
};

exports.create = (req, res) => {
  // Validate request
  if (Object.keys(req.body).length === 0) {
    res.status(400).send({
      message: "Content can not be empty!",
    });
  }

  const course = {
    nik: req.user.nik,
    judul_kursus: req.body.judul_kursus,
    penyelenggara: req.body.penyelenggara,
    tempat: req.body.tempat,
    tanggal_mulai: req.body.tanggal_mulai,
    tanggal_selesai: req.body.tanggal_selesai,
    lama_kegiatan: req.body.lama_kegiatan,
    sertifikat: req.body.sertifikat,
    role: req.body.role,
  };

  models.tb_p_kursus
    .create(course)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the course.",
      });
    });
};

exports.update = (req, res) => {
  // Validate Request
  if (Object.keys(req.body).length === 0) {
    res.status(400).send({
      message: "Content can not be empty!",
    });
  }

  const newCourse = {
    nik: req.user.nik,
    judul_kursus: req.body.judul_kursus,
    penyelenggara: req.body.penyelenggara,
    tempat: req.body.tempat,
    tanggal_mulai: req.body.tanggal_mulai,
    tanggal_selesai: req.body.tanggal_selesai,
    lama_kegiatan: req.body.lama_kegiatan,
    sertifikat: req.body.sertifikat,
    role: req.body.role,
  };

  models.tb_p_kursus
    .update(newCourse, {
      where: {
        id_kursus: req.params.id_kursus,
      },
    })
    .then((data) => {
      res.status(200).json(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: "Error updating course with id: " + req.params.id_kursus,
      });
    });
};

exports.delete = (req, res) => {
  models.tb_p_kursus
    .destroy({
      where: {
        id_kursus: req.params.id_kursus,
      },
    })
    .then((data) => {
      res.send({ message: `Course was deleted successfully!` });
    })
    .catch((err) => {
      res.status(500).send({
        message: "Error deleting course with id: " + req.params.id_kursus,
      });
    });
};
