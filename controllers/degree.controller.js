const models = require("../models/init-models");

exports.findAll = (req, res) => {
  models.tb_m_gelar
    .findAll()
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Error retrieving all degree data",
      });
    });
};

exports.create = (req, res) => {
  // Validate request
  if (Object.keys(req.body).length === 0) {
    res.status(400).send({
      message: "Content can not be empty!",
    });
  }

  const degree = {
    nama_gelar: req.body.nama_gelar,
  };

  models.tb_m_gelar
    .create(degree)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the degree.",
      });
    });
};

exports.findOne = (req, res) => {
  models.tb_m_gelar
    .findByPk(req.params.kd_gelar)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message ||
          "Error retrieving degree with id " + req.params.kd_gelar,
      });
    });
};

exports.update = (req, res) => {
  if (Object.keys(req.body).length === 0) {
    res.status(400).send({
      message: "Content can not be empty!",
    });
  }

  const degree = {
    nama_gelar: req.body.nama_gelar,
  };

  models.tb_m_gelar
    .update(degree, {
      where: {
        kd_gelar: req.params.kd_gelar,
      },
    })
    .then((num) => {
      if (num == 1) {
        res.send({ message: "Degree was updated successfully" });
      } else {
        res.send({
          message: `Cannot update degree with kd_gelar: ${req.params.kd_gelar}`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message ||
          "Error updating degree with id= " + req.params.kd_gelar,
      });
    });
};
