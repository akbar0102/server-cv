const models = require("../models/init-models");

exports.findAll = (req, res) => {
  models.tb_m_agama
    .findAll()
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Error retrieving all list religion data",
      });
    });
};

exports.create = (req, res) => {
  // Validate request
  if (Object.keys(req.body).length === 0) {
    res.status(400).send({
      message: "Content can not be empty!",
    });
  }

  const religion = {
    nama_agama: req.body.nama_agama,
  };

  models.tb_m_agama
    .create(religion)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the religion.",
      });
    });
};

exports.findOne = (req, res) => {
  models.tb_m_agama
    .findByPk(req.params.kd_agama)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: "Error retrieving religion with id " + req.params.kd_agama,
      });
    });
};

exports.update = (req, res) => {
  if (Object.keys(req.body).length === 0) {
    res.status(400).send({
      message: "Content can not be empty!",
    });
  }

  const religion = {
    nama_agama: req.body.nama_agama,
  };

  models.tb_m_agama
    .update(religion, {
      where: {
        kd_agama: req.params.kd_agama,
      },
    })
    .then((num) => {
      if (num == 1) {
        res.send({ message: "Religion was update successfully!" });
      } else {
        res.send({
          message: `Cannot update religion with kd_agama: ${req.params.kd_agama}. Maybe religion was not found!`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message ||
          "Could not update religion with id=" + req.params.kd_agama,
      });
    });
};
