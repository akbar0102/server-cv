const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const models = require("../models/init-models");
const { Op } = require("sequelize");

exports.create = async (req, res) => {
  //validasi pakai joi
  // Validate request
  if (!req.body) {
    res.status(400).send({
      message: "Content can not be empty!",
    });
  }

  // cek apakah user sudah ada/belum 
  const userExist = await models.tb_user.findOne({
    where:{
      [Op.or]: [
        { nik: req.body.nik },
        { email: req.body.email }
      ]
    }
  });
  if(userExist) return res.status(400).send({
    message: 'NIK atau email sudah terdaftar'
  });

  // hash password
  const salt = await bcrypt.genSalt(10);
  const hashedPassword = await bcrypt.hash(req.body.password, salt);

  //create user
  const user = {
    nik: req.body.nik,
    email: req.body.email,
    password: hashedPassword,
  };

  models.tb_user.create(user)
    .then(data => {
      res.send(data);
    }).catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating user."
      });
    });
};

exports.getOne = async (req, res) => {
  const user = await models.tb_user.findOne({
    where: {
      nik: req.body.nik
    }
  });
  if(!user) return res.status(400).send({
    message: 'NIK belum terdaftar'
  });

  const validPass = await bcrypt.compare(
    req.body.password,
    user.password
  )

  if (!validPass) return res.status(400).send({message: "Invalid password"});
  //create and assign token
  const token = jwt.sign({ nik: user.nik }, process.env.TOKEN_SECRET);
  res.header("auth-token", token).send({
    message: "success",
    token: token
  });
};
