var createError = require("http-errors");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
var dotenv = require("dotenv").config();
var cors = require('cors');

var indexRouter = require("./routes/index");
var personalRouter = require("./routes/personal.route");
var userRouter = require("./routes/user.route");
var courseRouter = require("./routes/course.route");
var eduRouter = require("./routes/education.route");
var projectRouter = require("./routes/project.route");
var workRouter = require("./routes/work.route");
var achievementRouter = require("./routes/achievement.route");
var masterRouter = require('./routes/master.route');

var app = express();

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");

app.use(logger("dev"));
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use("/", indexRouter);
app.use("/api", personalRouter);
app.use("/api", userRouter);
app.use("/api", courseRouter);
app.use("/api", eduRouter);
app.use("/api", projectRouter);
app.use("/api", workRouter);
app.use("/api", achievementRouter);
app.use("/api", masterRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

module.exports = app;
