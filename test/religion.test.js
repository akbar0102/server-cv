const chai = require("chai");
const chaiHttp = require("chai-http");
const expect = chai.expect;
const app = require("../app");
const sinon = require("sinon");
const models = require("../models/init-models");
const religionController = require("../controllers/religion.controller");

chai.use(chaiHttp);

describe("Master Religion API", () => {
  let token = "";
  before((done) => {
    chai
      .request(app)
      .post("/api/cv/login")
      .send({
        nik: "BSP2100001",
        password: "12345",
      })
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(200);
        expect(res.body).to.have.property("token");
        token = res.body.token;
        done();
      });
  });

  //get all => 200
  it("should retrieve list of all religion data", (done) => {
    chai
      .request(app)
      .get("/api/cv/master/religion")
      .set("auth-token", token)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(200);
        done();
      });
  });

  //get all => 401
  it("should retrieve list of all religion data, but no token", (done) => {
    chai
      .request(app)
      .get("/api/cv/master/religion")
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(401);
        done();
      });
  });

  //get all => 500
  it("should return error retrieving religion data", (done) => {
    sinon.stub(models.tb_m_agama, "findAll").rejects({ message: "" });

    chai
      .request(app)
      .get("/api/cv/master/religion")
      .set("auth-token", token)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(500);
        sinon.restore();
        done();
      });
  });

  //get one => 200
  it("should retrieve one data religion with parameter kd_agama", (done) => {
    chai
      .request(app)
      .get("/api/cv/master/religion/1")
      .set("auth-token", token)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(200);
        done();
      });
  });

  //get one => 401
  it("should retrieve one data religion with parameter kd_agama, but no token", (done) => {
    chai
      .request(app)
      .get("/api/cv/master/religion/1")
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(401);
        done();
      });
  });

  //get one => 500
  it("should return error retrieve one religion data with parameter kd_agama", (done) => {
    sinon.stub(models.tb_m_agama, "findOne").rejects({ message: "" });

    chai
      .request(app)
      .get("/api/cv/master/religion/1")
      .set("auth-token", token)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.be.have.status(500);
        sinon.restore();
        done();
      });
  });

  //post => 200
  it("should create religion data", (done) => {
    const religion = {
      nama_agama: "Testingtesting",
    };

    chai
      .request(app)
      .post("/api/cv/master/religion")
      .set("auth-token", token)
      .send(religion)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(200);
        done();
      });
  });

  //post => 400
  it("should post religion with error empty request body", () => {
    let req = {
      body: {},
    };

    let res = {
      status: sinon.stub().returnsThis(),
      send: sinon.spy(),
    };

    religionController.create(req, res);
    expect(res.send.calledOnce).to.be.true;
    sinon.assert.calledWith(res.status, 400);
    sinon.assert.calledWith(res.send, { message: "Content can not be empty!" });
  });

  //post => 401
  it("should create religion data, but no token", (done) => {
    const religion = {
      nama_agama: "Testingtesting",
    };

    chai
      .request(app)
      .post("/api/cv/master/religion")
      .send(religion)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(401);
        done();
      });
  });

  //post => 500
  it("should return error when post religion data", (done) => {
    sinon.stub(models.tb_m_agama, "create").rejects({ message: "" });

    const religion = {
      nama_agama: "Testingtesting",
    };

    chai
      .request(app)
      .post("/api/cv/master/religion")
      .set("auth-token", token)
      .send(religion)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.be.have.status(500);
        sinon.restore();
        done();
      });
  });

  //update => 200 (success)
  it("should update religion data with parameter kd_agama", (done) => {
    sinon.stub(models.tb_m_agama, "update").resolves(1);

    const religion = {
      nama_agama: "Testing",
    };

    chai
      .request(app)
      .put("/api/cv/master/religion/4")
      .set("auth-token", token)
      .send(religion)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(200);
        sinon.restore();
        done();
      });
  });

  //update => 200 (failed)
  it("should update religion data with parameter kd_agama", (done) => {
    sinon.stub(models.tb_m_agama, "update").resolves(0);

    const religion = {
      nama_agama: "Testing",
    };

    chai
      .request(app)
      .put("/api/cv/master/religion/4")
      .set("auth-token", token)
      .send(religion)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(200);
        done();
      });
  });

  //update => 400
  it("should update religion with error empty request body", () => {
    let req = {
      body: {},
      params: { kd_agama: "" },
    };

    let res = {
      status: sinon.stub().returnsThis(),
      send: sinon.spy(),
    };

    religionController.update(req, res);
    expect(res.send.calledOnce).to.be.true;
    sinon.assert.calledWith(res.status, 400);
    sinon.assert.calledWith(res.send, { message: "Content can not be empty!" });
    sinon.restore();
  });

  //update => 401
  it("should update religion data with parameter kd_agama, but no token", (done) => {
    const religion = {
      nama_agama: "Testing",
    };

    chai
      .request(app)
      .put("/api/cv/master/religion/4")
      .send(religion)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(401);
        done();
      });
  });

  //update => 500
  it("should return error when update religion data with parameter kd_agama", (done) => {
    sinon.stub(models.tb_m_agama, "update").rejects({ message: "" });

    const religion = {
      nama_agama: "Testing",
    };

    chai
      .request(app)
      .put("/api/cv/master/religion/4")
      .set("auth-token", token)
      .send(religion)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.be.have.status(500);
        sinon.restore();
        done();
      });
  });
});
