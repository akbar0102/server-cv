const chai = require("chai");
const chaiHttp = require("chai-http");
const expect = chai.expect;
const app = require("../app");
const sinon = require("sinon");
const models = require("../models/init-models");
const workController = require("../controllers/work.controller");

chai.use(chaiHttp);

describe("Work API", () => {
  let token = "";
  before((done) => {
    chai
      .request(app)
      .post("/api/cv/login")
      .send({
        nik: "BSP2100001",
        password: "12345",
      })
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(200);
        expect(res.body).to.have.property("token");
        token = res.body.token;
        done();
      });
  });

  //get all => 200
  it("should retrieve all work experience data for user who login", (done) => {
    chai
      .request(app)
      .get("/api/cv/work")
      .set("auth-token", token)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(200);
        done();
      });
  });

  //get all => 401
  it("should return access denied, without token", (done) => {
    chai
      .request(app)
      .get("/api/cv/work")
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(401);
        done();
      });
  });

  //get all => 500
  it("should return error retrieving all work experiences data", (done) => {
    sinon
      .stub(models.tb_p_pengalaman_kerja, "findAll")
      .rejects({ message: "" });

    chai
      .request(app)
      .get("/api/cv/work")
      .set("auth-token", token)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(500);
        sinon.restore();
        done();
      });
  });

  //get one => 200
  it("should retrieve one work experience data, then get all roles for work experience", (done) => {
    chai
      .request(app)
      .get("/api/cv/work/1")
      .set("auth-token", token)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(200);
        done();
      });
  });

  //get one => 401
  it("should retrieve one work experience data, then get all roles for work experience, but no token", (done) => {
    chai
      .request(app)
      .get("/api/cv/work/1")
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(401);
        done();
      });
  });

  // get one => 500
  it("should return error retrieve one work exp data with parameter id_pengalaman_kerja", (done) => {
    sinon
      .stub(models.tb_p_pengalaman_kerja, "findOne")
      .rejects({ message: "" });

    chai
      .request(app)
      .get("/api/cv/work/3")
      .set("auth-token", token)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.be.have.status(500);
        sinon.restore();
        done();
      });
  });

  //post => 200
  it("should post work experience data user who login", (done) => {
    const work = {
      nik: "BSP2100001",
      perusahaan: "PT ABC",
      tanggal_masuk: "2021-04-01",
      tanggal_keluar: "2021-04-10",
      status_pegawai: "Part Time Employee",
    };

    chai
      .request(app)
      .post("/api/cv/work")
      .set("auth-token", token)
      .send(work)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.be.have.status(200);
        done();
      });
  });

  // post => 400
  it("should post work exp with error empty request body", () => {
    let req = {
      user: { nik: "" },
      body: {},
    };

    let res = {
      status: sinon.stub().returnsThis(),
      send: sinon.spy(),
    };

    workController.create(req, res);
    expect(res.send.calledOnce).to.be.true;
    sinon.assert.calledWith(res.status, 400);
    sinon.assert.calledWith(res.send, { message: "Content can not be empty!" });
  });

  //post => 401
  it("should post work experience data user who login, but no token", (done) => {
    const work = {
      nik: "BSP2100001",
      perusahaan: "PT ABC",
      tanggal_masuk: "2021-04-01",
      tanggal_keluar: "2021-04-10",
      status_pegawai: "Part Time Employee",
    };

    chai
      .request(app)
      .post("/api/cv/work")
      .send(work)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.be.have.status(401);
        done();
      });
  });

  // post => 500
  it("should return error when post work exp data", (done) => {
    sinon.stub(models.tb_p_pengalaman_kerja, "create").rejects({ message: "" });

    const work = {
      nik: "BSP2100001",
      perusahaan: "PT ABC",
      tanggal_masuk: "2021-04-01",
      tanggal_keluar: "2021-04-10",
      status_pegawai: "Part Time Employee",
    };

    chai
      .request(app)
      .post("/api/cv/work")
      .set("auth-token", token)
      .send(work)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.be.have.status(500);
        sinon.restore();
        done();
      });
  });

  // update => 200 (success)
  it("should update work experience with parameter id_pengalaman_kerja", (done) => {
    sinon.stub(models.tb_p_pengalaman_kerja, "update").resolves(1);

    const work = {
      nik: "BSP2100001",
      perusahaan: "PT XYZ",
      tanggal_masuk: "2021-04-01",
      tanggal_keluar: "2021-04-11",
      status_pegawai: "Full Time Employee",
    };

    chai
      .request(app)
      .put("/api/cv/work/14")
      .set("auth-token", token)
      .send(work)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.be.have.status(200);
        sinon.restore();
        done();
      });
  });

  // update => 200 (failed)
  it("should update work experience with parameter id_pengalaman_kerja", (done) => {
    sinon.stub(models.tb_p_pengalaman_kerja, "update").resolves(0);

    const work = {
      nik: "BSP2100001",
      perusahaan: "PT XYZ",
      tanggal_masuk: "2021-04-01",
      tanggal_keluar: "2021-04-11",
      status_pegawai: "Full Time Employee",
    };

    chai
      .request(app)
      .put("/api/cv/work/14")
      .set("auth-token", token)
      .send(work)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.be.have.status(200);
        sinon.restore();
        done();
      });
  });

  // update => 400
  it("should update work exp with error empty request body", () => {
    let req = {
      body: {},
      user: { nik: "" },
      params: { id_pengalaman_kerja: "" },
    };

    let res = {
      status: sinon.stub().returnsThis(),
      send: sinon.spy(),
    };

    workController.update(req, res);
    expect(res.send.calledOnce).to.be.true;
    sinon.assert.calledWith(res.status, 400);
    sinon.assert.calledWith(res.send, { message: "Content can not be empty!" });
    sinon.restore();
  });

  //update => 401
  it("should update work experience with parameter id_pengalaman_kerja, but no token", (done) => {
    const work = {
      nik: "BSP2100001",
      perusahaan: "PT XYZ",
      tanggal_masuk: "2021-04-01",
      tanggal_keluar: "2021-04-11",
      status_pegawai: "Full Time Employee",
    };

    chai
      .request(app)
      .put("/api/cv/work/14")
      .send(work)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.be.have.status(401);
        done();
      });
  });

  // update => 500
  it("should return error when update work exp data with parameter id_pengalaman_kerja", (done) => {
    sinon.stub(models.tb_p_pengalaman_kerja, "update").rejects({ message: "" });

    const work = {
      nik: "BSP2100001",
      perusahaan: "PT XYZ",
      tanggal_masuk: "2021-04-01",
      tanggal_keluar: "2021-04-11",
      status_pegawai: "Full Time Employee",
    };

    chai
      .request(app)
      .put("/api/cv/work/10")
      .set("auth-token", token)
      .send(work)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.be.have.status(500);
        sinon.restore();
        done();
      });
  });

  // delete => 200 (success)
  it("should delete one work experience data with parameter id_pengalaman_kerja", (done) => {
    sinon.stub(models.tb_p_pengalaman_kerja, "destroy").resolves(1);
    chai
      .request(app)
      .delete("/api/cv/work/13")
      .set("auth-token", token)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.be.have.status(200);
        sinon.restore();
        done();
      });
  });

  // delete => 200 (failed)
  it("should delete one work experience data with parameter id_pengalaman_kerja", (done) => {
    sinon.stub(models.tb_p_pengalaman_kerja, "destroy").resolves(0);
    chai
      .request(app)
      .delete("/api/cv/work/13")
      .set("auth-token", token)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.be.have.status(200);
        sinon.restore();
        done();
      });
  });

  //delete => 401
  it("should delete one work experience data with parameter id_pengalaman_kerja, but no token", (done) => {
    chai
      .request(app)
      .delete("/api/cv/work/13")
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.be.have.status(401);
        done();
      });
  });

  // delete => 500
  it("should return error when delete work exp data with parameter id_pengalaman_kerja", (done) => {
    sinon
      .stub(models.tb_p_pengalaman_kerja, "destroy")
      .rejects({ message: "" });

    chai
      .request(app)
      .delete("/api/cv/work/99")
      .set("auth-token", token)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.be.have.status(500);
        sinon.restore();
        done();
      });
  });

  describe("Work Role API", () => {
    //get all roles work => 200
    it("should get all roles from specific work experience with parameter id_pengalaman_kerja", (done) => {
      chai
        .request(app)
        .get("/api/cv/work/roles/1")
        .set("auth-token", token)
        .end((err, res) => {
          expect(err).to.be.null;
          expect(res).to.have.status(200);
          done();
        });
    });

    // get all roles work => 500
    it("should get all roles from specific work exp with parameter id_role", (done) => {
      sinon
        .stub(models.tb_p_pengalaman_kerja_role, "findAll")
        .rejects({ message: "" });

      chai
        .request(app)
        .get("/api/cv/work/roles/1")
        .set("auth-token", token)
        .end((err, res) => {
          expect(err).to.be.null;
          expect(res).to.have.status(500);
          sinon.restore();
          done();
        });
    });

    //get one role => 200
    it("should get one of several roles from specific work experience, with parameter id_role", (done) => {
      chai
        .request(app)
        .get("/api/cv/work/role/1")
        .set("auth-token", token)
        .end((err, res) => {
          expect(err).to.be.null;
          expect(res).to.be.have.status(200);
          done();
        });
    });

    //get one role => 401
    it("should get one of several roles from specific work experience, with parameter id_role, but no token", (done) => {
      chai
        .request(app)
        .get("/api/cv/work/role/1")
        .end((err, res) => {
          expect(err).to.be.null;
          expect(res).to.be.have.status(401);
          done();
        });
    });

    // get one role => 500
    it("should return error retrieve one of several roles from specific work exp, with parameter id_role", (done) => {
      sinon
        .stub(models.tb_p_pengalaman_kerja_role, "findByPk")
        .rejects({ message: "" });

      chai
        .request(app)
        .get("/api/cv/work/role/1")
        .set("auth-token", token)
        .end((err, res) => {
          console.log(res.body);
          expect(err).to.be.null;
          expect(res).to.be.have.status(500);
          sinon.restore();
          done();
        });
    });

    //post role => 200
    it("should post data role for the work experience, with parameter id_pengalaman_kerja", (done) => {
      const wr = {
        id_pengalaman_kerja: 1,
        kd_jabatan: "RL004",
      };

      chai
        .request(app)
        .post("/api/cv/work/role/1")
        .set("auth-token", token)
        .send(wr)
        .end((err, res) => {
          expect(err).to.be.null;
          expect(res).to.be.have.status(200);
          done();
        });
    });

    // post role => 400
    it("should post data role for the work exp with error empty request body, with parameter id_pengalaman_kerja", () => {
      let req = {
        user: { nik: "" },
        body: {},
        params: { id_pengalaman_kerja: "" },
      };

      let res = {
        status: sinon.stub().returnsThis(),
        send: sinon.spy(),
      };

      workController.createRole(req, res);
      expect(res.send.calledOnce).to.be.true;
      sinon.assert.calledWith(res.status, 400);
      sinon.assert.calledWith(res.send, {
        message: "Content can not be empty!",
      });
    });

    //post role => 401
    it("should post data role for the work experience, with parameter id_pengalaman_kerja, but no token", (done) => {
      const wr = {
        id_pengalaman_kerja: 1,
        kd_jabatan: "RL004",
      };

      chai
        .request(app)
        .post("/api/cv/work/role/1")
        .send(wr)
        .end((err, res) => {
          expect(err).to.be.null;
          expect(res).to.be.have.status(401);
          done();
        });
    });

    // post role => 500
    it("should return error when post work exp role data, with parameter id_pengalaman_kerja", (done) => {
      sinon
        .stub(models.tb_p_pengalaman_kerja_role, "create")
        .rejects({ message: "" });

      const wr = {
        id_pengalaman_kerja: 1,
        kd_jabatan: "RL004",
      };

      chai
        .request(app)
        .post("/api/cv/work/role/1")
        .set("auth-token", token)
        .send(wr)
        .end((err, res) => {
          expect(err).to.be.null;
          expect(res).to.be.have.status(500);
          sinon.restore();
          done();
        });
    });

    // delete role => 200 (success)
    it("should delete one of several roles from specific work experience, with parameter id_role", (done) => {
      sinon.stub(models.tb_p_pengalaman_kerja_role, "destroy").resolves(1);
      chai
        .request(app)
        .delete("/api/cv/work/role/9")
        .set("auth-token", token)
        .end((err, res) => {
          expect(err).to.be.null;
          expect(res).to.be.have.status(200);
          sinon.restore();
          done();
        });
    });

    // delete role => 200 (failed)
    it("should delete one of several roles from specific work experience, with parameter id_role", (done) => {
      sinon.stub(models.tb_p_pengalaman_kerja_role, "destroy").resolves(0);
      chai
        .request(app)
        .delete("/api/cv/work/role/9")
        .set("auth-token", token)
        .end((err, res) => {
          expect(err).to.be.null;
          expect(res).to.be.have.status(200);
          sinon.restore();
          done();
        });
    });

    //delete role => 401
    it("should delete one of several roles from specific work experience, with parameter id_role, but no token", (done) => {
      chai
        .request(app)
        .delete("/api/cv/work/role/9")
        .end((err, res) => {
          expect(err).to.be.null;
          expect(res).to.be.have.status(401);
          done();
        });
    });

    // delete role => 500
    it("should return error when delete work exp role data with parameter id_role", (done) => {
      sinon
        .stub(models.tb_p_pengalaman_kerja_role, "destroy")
        .rejects({ message: "" });

      chai
        .request(app)
        .delete("/api/cv/work/role/99")
        .set("auth-token", token)
        .end((err, res) => {
          expect(err).to.be.null;
          expect(res).to.be.have.status(500);
          sinon.restore();
          done();
        });
    });
  });
});
