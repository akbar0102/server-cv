const chai = require("chai");
const chaiHttp = require("chai-http");
const expect = chai.expect;
const app = require("../app");
const sinon = require("sinon");
const models = require("../models/init-models");
const jobController = require("../controllers/job.controller");

chai.use(chaiHttp);

describe("Master Job/Role API", () => {
  let token = "";
  before((done) => {
    chai
      .request(app)
      .post("/api/cv/login")
      .send({
        nik: "BSP2100001",
        password: "12345",
      })
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(200);
        expect(res.body).to.have.property("token");
        token = res.body.token;
        done();
      });
  });

  //get all => 200
  it("should retrieve list of all roles data", (done) => {
    chai
      .request(app)
      .get("/api/cv/master/job")
      .set("auth-token", token)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(200);
        done();
      });
  });

  //get all => 401
  it("should retrieve list of all roles data, but no token", (done) => {
    chai
      .request(app)
      .get("/api/cv/master/job")
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(401);
        done();
      });
  });

  //get all => 500
  it("should return error retrieving job role data", (done) => {
    sinon
      .stub(models.tb_m_daftar_pekerjaan, "findAll")
      .rejects({ message: "" });

    chai
      .request(app)
      .get("/api/cv/master/job")
      .set("auth-token", token)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(500);
        sinon.restore();
        done();
      });
  });

  //get one => 200
  it("should retrieve one data role with parameter kd_jabatan", (done) => {
    chai
      .request(app)
      .get("/api/cv/master/job/RL001")
      .set("auth-token", token)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(200);
        done();
      });
  });

  //get one => 401
  it("should retrieve one data role with parameter kd_jabatan, but no token", (done) => {
    chai
      .request(app)
      .get("/api/cv/master/job/RL001")
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(401);
        done();
      });
  });

  //get one => 500
  it("should return error retrieve one job role data with parameter kd_jabatan", (done) => {
    sinon
      .stub(models.tb_m_daftar_pekerjaan, "findOne")
      .rejects({ message: "" });

    chai
      .request(app)
      .get("/api/cv/master/job/RL001")
      .set("auth-token", token)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.be.have.status(500);
        sinon.restore();
        done();
      });
  });

  //post => 200
  it("should create role data", (done) => {
    const role = {
      kd_jabatan: "RL017",
      nama_jabatan: "Farmer",
    };

    sinon.stub(models.tb_m_daftar_pekerjaan, "create").resolves(role);

    chai
      .request(app)
      .post("/api/cv/master/job")
      .set("auth-token", token)
      .send(role)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(200);
        sinon.restore();
        done();
      });
  });

  //post => 400
  it("should post job role with error empty request body", () => {
    let req = {
      body: {},
    };

    let res = {
      status: sinon.stub().returnsThis(),
      send: sinon.spy(),
    };

    jobController.create(req, res);
    expect(res.send.calledOnce).to.be.true;
    sinon.assert.calledWith(res.status, 400);
    sinon.assert.calledWith(res.send, { message: "Content can not be empty!" });
  });

  //post => 401
  it("should create role data, but no token", (done) => {
    const role = {
      kd_jabatan: "RL008",
      nama_jabatan: "Debugger",
    };
    chai
      .request(app)
      .post("/api/cv/master/job")
      .send(role)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(401);
        done();
      });
  });

  //post => 500
  it("should return error when post job role data", (done) => {
    sinon.stub(models.tb_m_daftar_pekerjaan, "create").rejects({ message: "" });

    const role = {
      kd_jabatan: "RL008",
      nama_jabatan: "Debugger",
    };

    chai
      .request(app)
      .post("/api/cv/master/job")
      .set("auth-token", token)
      .send(role)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.be.have.status(500);
        sinon.restore();
        done();
      });
  });

  //update => 200 (success)
  it("should update role data with parameter kd_jabatan", (done) => {
    sinon.stub(models.tb_m_daftar_pekerjaan, "update").resolves(1);

    const role = {
      nama_jabatan: "Gamers",
    };

    chai
      .request(app)
      .put("/api/cv/master/job/RL007")
      .set("auth-token", token)
      .send(role)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(200);
        sinon.restore();
        done();
      });
  });

  //update => 200 (failed)
  it("should update role data with parameter kd_jabatan", (done) => {
    sinon.stub(models.tb_m_daftar_pekerjaan, "update").resolves(0);

    const role = {
      nama_jabatan: "Gamers",
    };

    chai
      .request(app)
      .put("/api/cv/master/job/RL007")
      .set("auth-token", token)
      .send(role)
      .end((err, res) => {
        console.log(res.body);
        expect(err).to.be.null;
        expect(res).to.have.status(200);
        sinon.restore();
        done();
      });
  });

  //update => 400
  it("should update job role with error empty request body", () => {
    let req = {
      body: {},
      params: { kd_jabatan: "" },
    };

    let res = {
      status: sinon.stub().returnsThis(),
      send: sinon.spy(),
    };

    jobController.update(req, res);
    expect(res.send.calledOnce).to.be.true;
    sinon.assert.calledWith(res.status, 400);
    sinon.assert.calledWith(res.send, { message: "Content can not be empty!" });
    sinon.restore();
  });

  //update => 401
  it("should update role data with parameter kd_jabatan, but no token", (done) => {
    const role = {
      nama_jabatan: "Gamers",
    };

    chai
      .request(app)
      .put("/api/cv/master/job/RL007")
      .send(role)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(401);
        done();
      });
  });

  //update => 500
  it("should return error when update job role data with parameter kd_jabatan", (done) => {
    sinon.stub(models.tb_m_daftar_pekerjaan, "update").rejects({ message: "" });

    const role = {
      nama_jabatan: "Gamers",
    };

    chai
      .request(app)
      .put("/api/cv/master/job/RL007")
      .set("auth-token", token)
      .send(role)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.be.have.status(500);
        sinon.restore();
        done();
      });
  });
});
