const chai = require("chai");
const chaiHttp = require("chai-http");
const expect = chai.expect;
const app = require("../app");
const sinon = require("sinon");
const models = require("../models/init-models");
const degreeController = require("../controllers/degree.controller");

chai.use(chaiHttp);

describe("Master Degree API", () => {
  let token = "";
  before((done) => {
    chai
      .request(app)
      .post("/api/cv/login")
      .send({
        nik: "BSP2100001",
        password: "12345",
      })
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(200);
        expect(res.body).to.have.property("token");
        token = res.body.token;
        done();
      });
  });

  //get all => 200
  it("should retrieve list of all degree education data", (done) => {
    chai
      .request(app)
      .get("/api/cv/master/degree")
      .set("auth-token", token)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(200);
        done();
      });
  });

  //get all => 401
  it("should retrieve list of all degree education data, but no token", (done) => {
    chai
      .request(app)
      .get("/api/cv/master/degree")
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(401);
        done();
      });
  });

  //get all => 500
  it("should return error retrieving degrees data", (done) => {
    sinon.stub(models.tb_m_gelar, "findAll").rejects({ message: "" });

    chai
      .request(app)
      .get("/api/cv/master/degree")
      .set("auth-token", token)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(500);
        sinon.restore();
        done();
      });
  });

  //get one => 200
  it("should retrieve one data degree with parameter kd_gelar", (done) => {
    chai
      .request(app)
      .get("/api/cv/master/degree/1")
      .set("auth-token", token)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(200);
        done();
      });
  });

  //get one => 401
  it("should retrieve one data degree with parameter kd_gelar, but no token", (done) => {
    chai
      .request(app)
      .get("/api/cv/master/degree/1")
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(401);
        done();
      });
  });

  //get one => 500
  it("should return error retrieving one degree data with parameter kd_gelar", (done) => {
    sinon.stub(models.tb_m_gelar, "findByPk").rejects({ message: "" });

    chai
      .request(app)
      .get("/api/cv/master/degree/1")
      .set("auth-token", token)
      .end((err, res) => {
        console.log(res.body);
        expect(err).to.be.null;
        expect(res).to.have.status(500);
        sinon.restore();
        done();
      });
  });

  //post => 200
  it("should create degree education data", (done) => {
    const degree = {
      nama_gelar: "degree",
    };
    chai
      .request(app)
      .post("/api/cv/master/degree")
      .set("auth-token", token)
      .send(degree)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(200);
        done();
      });
  });

  //post => 400
  it("should post degree with error empty request body", () => {
    let req = {
      body: {},
    };

    let res = {
      status: sinon.stub().returnsThis(),
      send: sinon.spy(),
    };

    degreeController.create(req, res);
    expect(res.send.calledOnce).to.be.true;
    sinon.assert.calledWith(res.status, 400);
    sinon.assert.calledWith(res.send, { message: "Content can not be empty!" });
  });

  //post => 401
  it("should create degree education data, but no token", (done) => {
    const degree = {
      nama_gelar: "degree",
    };

    chai
      .request(app)
      .post("/api/cv/master/degree")
      .send(degree)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(401);
        done();
      });
  });

  //post => 500
  it("should return error when post degree data", (done) => {
    sinon.stub(models.tb_m_gelar, "create").rejects({ message: "" });

    const degree = {
      nama_gelar: "degree",
    };

    chai
      .request(app)
      .post("/api/cv/master/degree")
      .set("auth-token", token)
      .send(degree)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.be.have.status(500);
        sinon.restore();
        done();
      });
  });

  //update => 200 (success)
  it("should update degree data with parameter kd_gelar successfully update", (done) => {
    sinon.stub(models.tb_m_gelar, "update").resolves(1);

    const degree = {
      nama_gelar: "degreeA",
    };

    chai
      .request(app)
      .put("/api/cv/master/degree/68")
      .set("auth-token", token)
      .send(degree)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(200);
        console.log(res.body);
        sinon.restore();
        done();
      });
  });

  //update => 200 (fail)
  it("should update degree data with parameter kd_gelar failed update", (done) => {
    sinon.stub(models.tb_m_gelar, "update").resolves(0);

    const degree = {
      nama_gelar: "degreeA",
    };

    chai
      .request(app)
      .put("/api/cv/master/degree/8")
      .set("auth-token", token)
      .send(degree)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(200);
        console.log(res.body);
        sinon.restore();
        done();
      });
  });

  //update => 400
  it("should update degree with error empty request body", () => {
    let req = {
      body: {},
      params: { kd_gelar: "" },
    };

    let res = {
      status: sinon.stub().returnsThis(),
      send: sinon.spy(),
    };

    degreeController.update(req, res);
    expect(res.send.calledOnce).to.be.true;
    sinon.assert.calledWith(res.status, 400);
    sinon.assert.calledWith(res.send, { message: "Content can not be empty!" });
    sinon.restore();
  });

  //update => 401
  it("should update degree data with parameter kd_gelar, but no token", (done) => {
    const degree = {
      nama_gelar: "degreeA",
    };

    chai
      .request(app)
      .put("/api/cv/master/degree/8")
      .send(degree)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(401);
        done();
      });
  });

  //update => 500
  it("should return error when update degree data with parameter kd_gelar", (done) => {
    sinon.stub(models.tb_m_gelar, "update").rejects({ message: "" });

    const degree = {
      nama_gelar: "degreeA",
    };

    chai
      .request(app)
      .put("/api/cv/master/degree/10")
      .set("auth-token", token)
      .send(degree)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.be.have.status(500);
        sinon.restore();
        done();
      });
  });
});
