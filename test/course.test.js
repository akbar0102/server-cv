const chai = require("chai");
const chaiHttp = require("chai-http");
const expect = chai.expect;
const app = require("../app");
const sinon = require("sinon");
const models = require("../models/init-models");
const courseController = require("../controllers/course.controller");

chai.use(chaiHttp);

describe("Course API", () => {
  let token = "";
  before((done) => {
    chai
      .request(app)
      .post("/api/cv/login")
      .send({
        nik: "BSP2100001",
        password: "12345",
      })
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(200);
        expect(res.body).to.have.property("token");
        token = res.body.token;
        done();
      });
  });

  //get all => 200
  it("should retrieve all course data for user who login", (done) => {
    chai
      .request(app)
      .get("/api/cv/course")
      .set("auth-token", token)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(200);
        done();
      });
  });

  //get all => 401
  it("should return access denied, without token", (done) => {
    chai
      .request(app)
      .get("/api/cv/course")
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(401);
        done();
      });
  });

  //get all => 500
  it("should return error retrieving courses data user who login", (done) => {
    sinon.stub(models.tb_p_kursus, "findAll").rejects({ message: "" });

    //console.log(res.body);
    chai
      .request(app)
      .get("/api/cv/course")
      .set("auth-token", token)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(500);
        sinon.restore();
        done();
      });
  });

  //get one => 200
  it("should retrieve one course data with parameter id_kursus", (done) => {
    chai
      .request(app)
      .get("/api/cv/course/3")
      .set("auth-token", token)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(200);
        done();
      });
  });

  //get one => 401
  it("should retrieve one course data, but no token", (done) => {
    chai
      .request(app)
      .get("/api/cv/course/3")
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(401);
        done();
      });
  });

  //get one => 500
  it("should return error retrieving one course data with parameter id_kursus", (done) => {
    sinon.stub(models.tb_p_kursus, "findOne").rejects({ message: "" });

    chai
      .request(app)
      .get("/api/cv/course/1")
      .set("auth-token", token)
      .end((err, res) => {
        console.log(res.body);
        expect(err).to.be.null;
        expect(res).to.have.status(500);
        sinon.restore();
        done();
      });
  });

  //get one => null
  it("should return null on course data", (done) => {
    chai
      .request(app)
      .get("/api/cv/course/1")
      .set("auth-token", token)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(200);
        done();
      });
  });

  //post => 200
  it("should post course data user who login", (done) => {
    const course = {
      nik: "BSP2100001",
      judul_kursus: "CKO",
      penyelenggara: "Digitalent",
      tempat: "Online",
      tanggal_mulai: "2021-03-15",
      tanggal_selesai: "2021-04-23",
      lama_kegiatan: "30",
      sertifikat: "Ya",
      role: "RL004",
    };
    chai
      .request(app)
      .post("/api/cv/course")
      .set("auth-token", token)
      .send(course)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.be.have.status(200);
        done();
      });
  });

  //post => 400
  it("should post course with error empty request body", () => {
    let req = {
      user: { nik: "" },
      body: {},
    };

    let res = {
      status: sinon.stub().returnsThis(),
      send: sinon.spy(),
    };

    courseController.create(req, res);
    expect(res.send.calledOnce).to.be.true;
    sinon.assert.calledWith(res.status, 400);
    sinon.assert.calledWith(res.send, { message: "Content can not be empty!" });
  });

  //post => 500
  it("should return error when post course data", (done) => {
    sinon.stub(models.tb_p_kursus, "create").rejects({ message: "" });

    const course = {
      nik: "BSP2100001",
      judul_kursus: "CKO",
      penyelenggara: "Digitalent",
      tempat: "Online",
      tanggal_mulai: "2021-03-15",
      tanggal_selesai: "2021-04-23",
      lama_kegiatan: "30",
      sertifikat: "Ya",
      role: "RL004",
    };

    chai
      .request(app)
      .post("/api/cv/course")
      .set("auth-token", token)
      .send(course)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.be.have.status(500);
        sinon.restore();
        done();
      });
  });

  //update => 200
  it("should update course with parameter id_kursus", (done) => {
    const course = {
      nik: "BSP2100005",
      judul_kursus: "Big Data Python",
      penyelenggara: "Digitalent",
      tempat: "Online",
      tanggal_mulai: "2019-08-01",
      tanggal_selesai: "2021-04-01",
      lama_kegiatan: "30",
      sertifikat: "Ya",
      role: "RL004",
    };

    chai
      .request(app)
      .put("/api/cv/education/17")
      .set("auth-token", token)
      .send(course)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.be.have.status(200);
        done();
      });
  });

  //update => 400
  it("should update course with error empty request body", () => {
    let req = {
      user: { nik: "" },
      body: {},
      params: { id_kursus: "" },
    };

    let res = {
      status: sinon.stub().returnsThis(),
      send: sinon.spy(),
    };

    courseController.update(req, res);
    expect(res.send.calledOnce).to.be.true;
    sinon.assert.calledWith(res.status, 400);
    sinon.assert.calledWith(res.send, { message: "Content can not be empty!" });
    sinon.restore();
  });

  //update => 500
  it("should return error when update course data with parameter id_kursus", (done) => {
    sinon.stub(models.tb_p_kursus, "update").rejects({ message: "" });

    const course = {
      nik: "BSP2100005",
      judul_kursus: "Big Data Python",
      penyelenggara: "Digitalent",
      tempat: "Online",
      tanggal_mulai: "2019-08-01",
      tanggal_selesai: "2021-04-01",
      lama_kegiatan: "30",
      sertifikat: "Ya",
      role: "RL004",
    };

    chai
      .request(app)
      .put("/api/cv/course/10")
      .set("auth-token", token)
      .send(course)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.be.have.status(500);
        sinon.restore();
        done();
      });
  });

  //delete => 200
  it("should delete course data with parameter id_kursus", (done) => {
    chai
      .request(app)
      .delete("/api/cv/education/18")
      .set("auth-token", token)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.be.have.status(200);
        done();
      });
  });

  //delete => 500
  it("should return error when delete course data with parameter id_kursus", (done) => {
    sinon.stub(models.tb_p_kursus, "destroy").rejects({ message: "" });

    chai
      .request(app)
      .delete("/api/cv/course/17")
      .set("auth-token", token)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.be.have.status(500);
        sinon.restore();
        done();
      });
  });
});
