const chai = require("chai");
const chaiHttp = require("chai-http");
const expect = chai.expect;
const app = require("../app");

chai.use(chaiHttp);

describe("User API", () => {
  describe("Register User", () => {
    it("should create a new user", (done) => {
      const user = {
        nik: "BSP2100040",
        email: "qqq@gmail.com",
        password: "11111",
      };
      chai
        .request(app)
        .post("/api/cv/register")
        .send(user)
        .end((err, res) => {
          expect(err).to.be.null;
          expect(res).to.have.status(200);
          done();
        });
    });

    it("should return user already in database", (done) => {
      const user = {
        nik: "BSP2100001",
        email: "yuda@gmail.com",
        password: "12345",
      };
      chai
        .request(app)
        .post("/api/cv/register")
        .send(user)
        .end((err, res) => {
          expect(err).to.be.null;
          expect(res).to.have.status(400);
          done();
        });
    });
  });

  describe("Login User", () => {
    it("should return token, when user nik and password match in database", (done) => {
      chai
        .request(app)
        .post("/api/cv/login")
        .send({
          nik: "BSP2100001",
          password: "12345",
        })
        .end((err, res) => {
          expect(err).to.be.null;
          expect(res).to.have.status(200);
          expect(res.body).to.have.property("token");
          done();
        });
    });

    it("should return user not registered", (done) => {
      chai
        .request(app)
        .post("/api/cv/login")
        .send({
          nik: "BSP2100011",
          password: "12345",
        })
        .end((err, res) => {
          expect(err).to.be.null;
          expect(res).to.have.status(400);
          done();
        });
    });

    it("should return wrong password", (done) => {
      chai
        .request(app)
        .post("/api/cv/login")
        .send({
          nik: "BSP2100001",
          password: "hahaha",
        })
        .end((err, res) => {
          expect(err).to.be.null;
          expect(res).to.have.status(400);
          done();
        });
    });
  });
});
