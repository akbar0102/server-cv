const chai = require('chai');
const chaiHttp = require('chai-http');
const expect = chai.expect;
const app = require('../app');
const fs = require('fs');

chai.use(chaiHttp);

describe('Person API', () => {
    let token = '';
    before(done => {
        chai.request(app).post('/api/cv/login')
            .send({
                'nik': 'BSP2100001',
                'password': '12345'
            })
            .end((err, res) => {
                expect(err).to.be.null;
                expect(res).to.have.status(200);
                expect(res.body).to.have.property('token');
                token = res.body.token;
                done();
            });
    });

    it('should return access denied without token', (done) => {
        chai.request(app).get('/api/cv/person')
            .send({
                'nik': 'BSP2100001'
            })
            .end((err, res) => {
                expect(err).to.be.null;
                expect(res).to.have.status(401);
                expect(res.body).to.be.an('Object');
                expect(res.body).to.have.property('message');
                done();
            })
    });

    it('should retrieve person data who login', (done) => {
        chai.request(app).get('/api/cv/person')
            .set('auth-token', token)
            .send({
                'nik': 'BSP2100001'
            })
            .end((err, res) => {
                expect(err).to.be.null;
                expect(res).to.have.status(200);
                expect(res.body).to.be.an('Object');
                done();
            });
    });

    it('should update data person', (done) => {
        const image = __dirname + '/codingceria.png';
        const person = {
            nama_depan: 'Yuda',
            nama_belakang: 'Permana',
            tempat_lahir: 'Bandung',
            tanggal_lahir: '1989-03-05',
            agama: 1,
            jenis_kelamin: 'L',
            kesehatan: 'Good',
            foto: image,
            role_terakhir: 'RL001',
            list_profile: '1. Almost 9 year experience as professional IT Programmer, Tester, and Facilitator \
            2. Goal-oriented individual with good analytic thinking, fast leaner, result oriented',
            list_ptech: '1. Programming Language: Java (Android),  Java (J2EE), SQL, Jsp/Servlet, C#, Python, Ruby, JavaScript \
            2.Framework : Struts, Spring, Jasper Reports, POI, ASP.NET, ASP.NET MVC 3, ASP.NET MVC 4, Web API, Terasoluna, MyBatis, Hibernet, VueJS, NodeJS'
        }
        chai.request(app)
            .put('/api/cv/person')
            .set('auth-token', token)
            .set('Content-Type', 'application/x-www-form-urlencoded')
            .field('nama_depan', person.nama_depan)
            .field('nama_belakang', person.nama_belakang)
            .field('tempat_lahir', person.tempat_lahir)
            .field('tanggal_lahir', person.tanggal_lahir)
            .field('agama', person.agama)
            .field('jenis_kelamin', person.jenis_kelamin)
            .field('kesehatan', person.kesehatan)
            .attach('image', fs.readFileSync(person.foto), 'codingceria.png')
            .field('role_terakhir', person.role_terakhir)
            .field('list_profile', person.list_profile)
            .field('list_ptech', person.list_ptech)
            .end((err, res) => {
                expect(err).to.be.null;
                expect(res).to.have.status(200);
                done();
            })
    });

    it('should generate resume docx', (done) => {
        chai.request(app).get('/api/cv/person/generate')
            .set('auth-token', token)
            .end((err, res) => {
                expect(err).to.be.null;
                expect(res).to.have.status(200);
                done();
            });
    });
});