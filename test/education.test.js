const chai = require("chai");
const chaiHttp = require("chai-http");
const expect = chai.expect;
const app = require("../app");
const sinon = require("sinon");
const models = require("../models/init-models");
const eduController = require("../controllers/education.controller");

chai.use(chaiHttp);

describe("Education API", () => {
  let token = "";
  before((done) => {
    chai
      .request(app)
      .post("/api/cv/login")
      .send({
        nik: "BSP2100001",
        password: "12345",
      })
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(200);
        expect(res.body).to.have.property("token");
        token = res.body.token;
        done();
      });
  });

  //get all => 200
  it("should retrieve all education data for user who login", (done) => {
    chai
      .request(app)
      .get("/api/cv/education")
      .set("auth-token", token)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(200);
        done();
      });
  });

  //get all => 401
  it("should return access denied, without token", (done) => {
    chai
      .request(app)
      .get("/api/cv/education")
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(401);
        done();
      });
  });

  //get all => 500
  it("should return error retrieving educations data", (done) => {
    sinon.stub(models.tb_p_pendidikan, "findAll").rejects({ message: "" });

    chai
      .request(app)
      .get("/api/cv/education")
      .set("auth-token", token)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(500);
        sinon.restore();
        done();
      });
  });

  //get one => 200
  it("should retrieve one education data", (done) => {
    chai
      .request(app)
      .get("/api/cv/education/3")
      .set("auth-token", token)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.be.have.status(200);
        done();
      });
  });

  //get one => 401
  it("should retrieve one education data, but no token", (done) => {
    chai
      .request(app)
      .get("/api/cv/education/3")
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.be.have.status(401);
        done();
      });
  });

  //get one => 500
  it("should return error retrieve one education data with parameter id_pendidikan", (done) => {
    sinon.stub(models.tb_p_pendidikan, "findOne").rejects({ message: "" });

    chai
      .request(app)
      .get("/api/cv/education/3")
      .set("auth-token", token)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.be.have.status(500);
        sinon.restore();
        done();
      });
  });

  //post => 200
  it("should post education data user who login", (done) => {
    const education = {
      nik: "BSP2100001",
      instansi: "SMA 10",
      tingkat_pendidikan: "3",
      jurusan: "IPA",
      tahun_masuk: "2008",
      tahun_keluar: "2011",
    };

    chai
      .request(app)
      .post("/api/cv/education")
      .set("auth-token", token)
      .send(education)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.be.have.status(200);
        done();
      });
  });

  //post => 400
  it("should post education with error empty request body", () => {
    let req = {
      user: { nik: "" },
      body: {},
    };

    let res = {
      status: sinon.stub().returnsThis(),
      send: sinon.spy(),
    };

    eduController.create(req, res);
    expect(res.send.calledOnce).to.be.true;
    sinon.assert.calledWith(res.status, 400);
    sinon.assert.calledWith(res.send, { message: "Content can not be empty!" });
  });

  //post => 401
  it("should post education data user who login, but no token", (done) => {
    const education = {
      nik: "BSP2100001",
      instansi: "SMA 10",
      tingkat_pendidikan: "3",
      jurusan: "IPA",
      tahun_masuk: "2008",
      tahun_keluar: "2011",
    };

    chai
      .request(app)
      .post("/api/cv/education")
      .send(education)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.be.have.status(401);
        done();
      });
  });

  //post => 500
  it("should return error when post education data", (done) => {
    sinon.stub(models.tb_p_pendidikan, "create").rejects({ message: "" });

    const education = {
      nik: "BSP2100001",
      instansi: "SMA 10",
      tingkat_pendidikan: "3",
      jurusan: "IPA",
      tahun_masuk: "2008",
      tahun_keluar: "2011",
    };

    chai
      .request(app)
      .post("/api/cv/education")
      .set("auth-token", token)
      .send(education)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.be.have.status(500);
        sinon.restore();
        done();
      });
  });

  //update => 200 (success)
  it("should update education with parameter id_pendidikan", (done) => {
    sinon.stub(models.tb_p_pendidikan, "update").resolves(1);

    const education = {
      nik: "BSP2100001",
      instansi: "SMA 5",
      tingkat_pendidikan: "3",
      jurusan: "IPS",
      tahun_masuk: "2008",
      tahun_keluar: "2011",
    };

    chai
      .request(app)
      .put("/api/cv/education/17")
      .set("auth-token", token)
      .send(education)
      .end((err, res) => {
        console.log(res.body);
        expect(err).to.be.null;
        expect(res).to.be.have.status(200);
        sinon.restore();
        done();
      });
  });

  //update => 200 (failed)
  it("should update education with parameter id_pendidikan", (done) => {
    sinon.stub(models.tb_p_pendidikan, "update").resolves(0);

    const education = {
      nik: "BSP2100001",
      instansi: "SMA 5",
      tingkat_pendidikan: "3",
      jurusan: "IPS",
      tahun_masuk: "2008",
      tahun_keluar: "2011",
    };

    chai
      .request(app)
      .put("/api/cv/education/17")
      .set("auth-token", token)
      .send(education)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.be.have.status(200);
        sinon.restore();
        done();
      });
  });

  //update => 400
  it("should update education with error empty request body", () => {
    let req = {
      body: {},
      user: { nik: "" },
      params: { id_pendidikan: "" },
    };

    let res = {
      status: sinon.stub().returnsThis(),
      send: sinon.spy(),
    };

    eduController.update(req, res);
    expect(res.send.calledOnce).to.be.true;
    sinon.assert.calledWith(res.status, 400);
    sinon.assert.calledWith(res.send, { message: "Content can not be empty!" });
    sinon.restore();
  });

  //update => 401
  it("should update education with parameter id_pendidikan, but no token", (done) => {
    const education = {
      nik: "BSP2100001",
      instansi: "SMA 5",
      tingkat_pendidikan: "3",
      jurusan: "IPS",
      tahun_masuk: "2008",
      tahun_keluar: "2011",
    };

    chai
      .request(app)
      .put("/api/cv/education/17")
      .send(education)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.be.have.status(401);
        done();
      });
  });

  //update => 500
  it("should return error when update education data with parameter id_pendidikan", (done) => {
    sinon.stub(models.tb_p_pendidikan, "update").rejects({ message: "" });

    const education = {
      nik: "BSP2100001",
      instansi: "SMA 5",
      tingkat_pendidikan: "3",
      jurusan: "IPS",
      tahun_masuk: "2008",
      tahun_keluar: "2011",
    };

    chai
      .request(app)
      .put("/api/cv/education/10")
      .set("auth-token", token)
      .send(education)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.be.have.status(500);
        sinon.restore();
        done();
      });
  });

  //delete => 200 (success)
  it("should delete education data with parameter id_pendidikan", (done) => {
    sinon.stub(models.tb_p_pendidikan, "destroy").resolves(1);

    chai
      .request(app)
      .delete("/api/cv/education/0")
      .set("auth-token", token)
      .end((err, res) => {
        console.log(res.body);
        expect(err).to.be.null;
        expect(res).to.be.have.status(200);
        sinon.restore();
        done();
      });
  });

  //delete => 200 (failed)
  it("should delete education data with parameter id_pendidikan", (done) => {
    sinon.stub(models.tb_p_pendidikan, "destroy").resolves(0);

    chai
      .request(app)
      .delete("/api/cv/education/100")
      .set("auth-token", token)
      .end((err, res) => {
        console.log(res.body);
        expect(err).to.be.null;
        expect(res).to.be.have.status(200);
        sinon.restore();
        done();
      });
  });

  //delete => 401
  it("should delete education data with parameter id_pendidikan, but no token", (done) => {
    chai
      .request(app)
      .delete("/api/cv/education/18")
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.be.have.status(401);
        done();
      });
  });

  //delete => 500
  it("should return error when delete achievement data with parameter id_prestasi", (done) => {
    sinon.stub(models.tb_p_pendidikan, "destroy").rejects({ message: "" });

    chai
      .request(app)
      .delete("/api/cv/education/99")
      .set("auth-token", token)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.be.have.status(500);
        sinon.restore();
        done();
      });
  });
});
