const chai = require("chai");
const chaiHttp = require("chai-http");
const expect = chai.expect;
const app = require("../app");
const sinon = require("sinon");
const models = require("../models/init-models");
const achievementController = require("../controllers/achievement.controller");

chai.use(chaiHttp);

describe("Achievement API", () => {
  let token = "";
  before((done) => {
    chai
      .request(app)
      .post("/api/cv/login")
      .send({
        nik: "BSP2100001",
        password: "12345",
      })
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(200);
        expect(res.body).to.have.property("token");
        token = res.body.token;
        done();
      });
  });

  //get all => 200
  it("should retrieve all achievement data for user who login", (done) => {
    chai
      .request(app)
      .get("/api/cv/achievement")
      .set("auth-token", token)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(200);
        done();
      });
  });

  //get all => 401
  it("should return access denied, without token", (done) => {
    chai
      .request(app)
      .get("/api/cv/achievement")
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(401);
        done();
      });
  });

  //get all => 500
  it("should return error retrieving achievements data user who login", (done) => {
    sinon.stub(models.tb_p_prestasi, "findAll").rejects({ message: "" });

    //console.log(res.body);
    chai
      .request(app)
      .get("/api/cv/achievement")
      .set("auth-token", token)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(500);
        sinon.restore();
        done();
      });
  });

  //get one => 200
  it("should retrieve one achievement data with parameter id_prestasi", (done) => {
    chai
      .request(app)
      .get("/api/cv/achievement/1")
      .set("auth-token", token)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(200);
        expect(res.body).to.be.an("Object");
        expect(res.body).to.have.all.keys(
          "id_prestasi",
          "nik",
          "kd_jabatan",
          "detail",
          "kd_jabatan_tb_m_daftar_pekerjaan"
        );
        done();
      });
  });

  //get one => 500
  it("should return error retrieving one achievement data with parameter id_prestasi", (done) => {
    sinon.stub(models.tb_p_prestasi, "findOne").rejects({ message: "" });

    chai
      .request(app)
      .get("/api/cv/achievement/1")
      .set("auth-token", token)
      .end((err, res) => {
        console.log(res.body);
        expect(err).to.be.null;
        expect(res).to.have.status(500);
        sinon.restore();
        done();
      });
  });

  //get one => 401
  it("should retrieve one achievement data with parameter id_prestasi, but without token", (done) => {
    chai
      .request(app)
      .get("/api/cv/achievement/1")
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(401);
        done();
      });
  });

  //get one => null
  it("should return null on achievement data", (done) => {
    chai
      .request(app)
      .get("/api/cv/achievement/3")
      .set("auth-token", token)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(200);
        done();
      });
  });

  //post => 200
  it("should post achievement data user who login", (done) => {
    const achievement = {
      nik: "BSP2100001",
      kd_jabatan: "RL002",
      detail: "Creating ebook training",
    };

    chai
      .request(app)
      .post("/api/cv/achievement")
      .set("auth-token", token)
      .send(achievement)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.be.have.status(200);
        done();
      });
  });

  //post => 400
  it("should post error with empty request body", () => {
    let req = {
      user: { nik: "" },
      body: {},
    };

    let res = {
      status: sinon.stub().returnsThis(),
      send: sinon.spy(),
    };

    achievementController.create(req, res);
    expect(res.send.calledOnce).to.be.true;
    sinon.assert.calledWith(res.status, 400);
    sinon.assert.calledWith(res.send, { message: "Content can not be empty!" });
  });

  //post => 500
  it("should return error when post achievement data", (done) => {
    sinon.stub(models.tb_p_prestasi, "create").rejects({ message: "" });

    const achievement = {
      nik: "BSP2100001",
      kd_jabatan: "RL002",
      detail: "Creating ebook training",
    };

    chai
      .request(app)
      .post("/api/cv/achievement")
      .set("auth-token", token)
      .send(achievement)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.be.have.status(500);
        sinon.restore();
        done();
      });
  });

  it("should post achievement data user who login, but no token", (done) => {
    const achievement = {
      nik: "BSP2100001",
      kd_jabatan: "RL002",
      detail: "Creating ebook training",
    };

    chai
      .request(app)
      .post("/api/cv/achievement")
      .send(achievement)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.be.have.status(401);
        done();
      });
  });

  //update => 200
  it("should update achievement with parameter id_prestasi", (done) => {
    const achievement = {
      nik: "BSP2100001",
      kd_jabatan: "RL003",
      detail: "Leading team",
    };

    chai
      .request(app)
      .put("/api/cv/achievement/10")
      .set("auth-token", token)
      .send(achievement)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.be.have.status(200);
        done();
      });
  });

  //update => 400
  it("should update error with empty request body", () => {
    let req = {
      user: { nik: "" },
      body: {},
      params: { id_prestasi: "" },
    };

    let res = {
      status: sinon.stub().returnsThis(),
      send: sinon.spy(),
    };

    achievementController.update(req, res);
    expect(res.send.calledOnce).to.be.true;
    sinon.assert.calledWith(res.status, 400);
    sinon.assert.calledWith(res.send, { message: "Content can not be empty!" });
    sinon.restore();
  });

  //update => 500
  it("should return error when update achievement data with parameter id_prestasi", (done) => {
    sinon.stub(models.tb_p_prestasi, "update").rejects({ message: "" });

    const achievement = {
      nik: "BSP2100001",
      kd_jabatan: "RL003",
      detail: "Leading team",
    };

    chai
      .request(app)
      .put("/api/cv/achievement/10")
      .set("auth-token", token)
      .send(achievement)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.be.have.status(500);
        sinon.restore();
        done();
      });
  });

  it("should update achievement with parameter id_prestasi, but no token", (done) => {
    const achievement = {
      nik: "BSP2100001",
      kd_jabatan: "RL003",
      detail: "Leading team",
    };

    chai
      .request(app)
      .put("/api/cv/achievement/10")
      .send(achievement)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.be.have.status(401);
        done();
      });
  });

  //delete => 200
  it("should delete achievement data with parameter id_prestasi", (done) => {
    chai
      .request(app)
      .delete("/api/cv/achievement/17")
      .set("auth-token", token)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.be.have.status(200);
        done();
      });
  });

  //delete => 500
  it("should return error when delete achievement data with parameter id_prestasi", (done) => {
    sinon.stub(models.tb_p_prestasi, "destroy").rejects({ message: "" });

    chai
      .request(app)
      .delete("/api/cv/achievement/17")
      .set("auth-token", token)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.be.have.status(500);
        sinon.restore();
        done();
      });
  });

  it("should delete achievement data with parameter id_prestasi, but no token", (done) => {
    chai
      .request(app)
      .delete("/api/cv/achievement/17")
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.be.have.status(401);
        done();
      });
  });
});
