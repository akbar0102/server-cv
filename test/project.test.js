const chai = require("chai");
const chaiHttp = require("chai-http");
const expect = chai.expect;
const app = require("../app");
const sinon = require("sinon");
const models = require("../models/init-models");
const projectController = require("../controllers/project.controller");

chai.use(chaiHttp);

describe("Project API", () => {
  let token = "";
  before((done) => {
    chai
      .request(app)
      .post("/api/cv/login")
      .send({
        nik: "BSP2100001",
        password: "12345",
      })
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(200);
        expect(res.body).to.have.property("token");
        token = res.body.token;
        done();
      });
  });

  //get all => 200
  it("should retrieve all project data for user who login", (done) => {
    chai
      .request(app)
      .get("/api/cv/project")
      .set("auth-token", token)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(200);
        done();
      });
  });

  //get all => 401
  it("should return access denied, without token", (done) => {
    chai
      .request(app)
      .get("/api/cv/project")
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(401);
        done();
      });
  });

  //get all => 500
  it("should return error retrieving projects data", (done) => {
    sinon.stub(models.tb_project, "findAll").rejects({ message: "" });

    chai
      .request(app)
      .get("/api/cv/project")
      .set("auth-token", token)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(500);
        sinon.restore();
        done();
      });
  });

  //get one => 200
  it("should retrieve one project data, then get all roles for the project", (done) => {
    chai
      .request(app)
      .get("/api/cv/project/1")
      .set("auth-token", token)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(200);
        done();
      });
  });

  //get one => 401
  it("should retrieve one project data, then get all roles for the project, but no token", (done) => {
    chai
      .request(app)
      .get("/api/cv/project/1")
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(401);
        done();
      });
  });

  //get one => 500
  it("should return error retrieve one project data with parameter id_project", (done) => {
    sinon.stub(models.tb_project, "findOne").rejects({ message: "" });

    chai
      .request(app)
      .get("/api/cv/project/3")
      .set("auth-token", token)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.be.have.status(500);
        sinon.restore();
        done();
      });
  });

  //post => 200
  it("should post project data user who login", (done) => {
    const project = {
      nik: "BSP2100001",
      nama_project: "aplikasi mobile",
      tanggal_mulai: "2021-04-01",
      tanggal_selesai: "2021-05-01",
      customer: "Koperasi",
      end_customer: "koperasi",
      project_site: "kantor",
      deskripsi_project: "membuat aplikasi mobile",
      tech_info: "pakai react native",
      other_info: "tes",
      project_lang: "javascript",
      project_framework: "react",
      project_app: "mobile",
      project_tool: "vscode",
      project_db: "mysql",
      project_os: "android",
      project_server: "ubuntu",
    };

    chai
      .request(app)
      .post("/api/cv/project")
      .set("auth-token", token)
      .send(project)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.be.have.status(200);
        done();
      });
  });

  //post => 400
  it("should post project with error empty request body", () => {
    let req = {
      user: { nik: "" },
      body: {},
    };

    let res = {
      status: sinon.stub().returnsThis(),
      send: sinon.spy(),
    };

    projectController.create(req, res);
    expect(res.send.calledOnce).to.be.true;
    sinon.assert.calledWith(res.status, 400);
    sinon.assert.calledWith(res.send, { message: "Content can not be empty!" });
  });

  //post => 401
  it("should post project data user who login, but no token", (done) => {
    const project = {
      nik: "BSP2100001",
      nama_project: "aplikasi mobile",
      tanggal_mulai: "2021-04-01",
      tanggal_selesai: "2021-05-01",
      customer: "Koperasi",
      end_customer: "koperasi",
      project_site: "kantor",
      deskripsi_project: "membuat aplikasi mobile",
      tech_info: "pakai react native",
      other_info: "tes",
      project_lang: "javascript",
      project_framework: "react",
      project_app: "mobile",
      project_tool: "vscode",
      project_db: "mysql",
      project_os: "android",
      project_server: "ubuntu",
    };

    chai
      .request(app)
      .post("/api/cv/project")
      .send(project)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.be.have.status(401);
        done();
      });
  });

  //post => 500
  it("should return error when post project data", (done) => {
    sinon.stub(models.tb_project, "create").rejects({ message: "" });

    const project = {
      nik: "BSP2100001",
      nama_project: "aplikasi mobile",
      tanggal_mulai: "2021-04-01",
      tanggal_selesai: "2021-05-01",
      customer: "Koperasi",
      end_customer: "koperasi",
      project_site: "kantor",
      deskripsi_project: "membuat aplikasi mobile",
      tech_info: "pakai react native",
      other_info: "tes",
      project_lang: "javascript",
      project_framework: "react",
      project_app: "mobile",
      project_tool: "vscode",
      project_db: "mysql",
      project_os: "android",
      project_server: "ubuntu",
    };

    chai
      .request(app)
      .post("/api/cv/project")
      .set("auth-token", token)
      .send(project)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.be.have.status(500);
        sinon.restore();
        done();
      });
  });

  //update => 200 (success)
  it("should update project with parameter id_project", (done) => {
    sinon.stub(models.tb_project, "update").resolves(1);

    const project = {
      nik: "BSP2100001",
      nama_project: "aplikasi mobile",
      tanggal_mulai: "2021-04-01",
      tanggal_selesai: "2021-05-01",
      customer: "Koperasi A",
      end_customer: "koperasi B",
      project_site: "kantorr",
      deskripsi_project: "membuat aplikasi mobile react",
      tech_info: "pakai react native",
      other_info: "tes",
      project_lang: "javascript, sql",
      project_framework: "react",
      project_app: "mobile",
      project_tool: "vscode",
      project_db: "mysql",
      project_os: "android",
      project_server: "ubuntu",
    };

    chai
      .request(app)
      .put("/api/cv/project/10")
      .set("auth-token", token)
      .send(project)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.be.have.status(200);
        sinon.restore();
        done();
      });
  });

  //update => 200 (failed)
  it("should update project with parameter id_project", (done) => {
    sinon.stub(models.tb_project, "update").resolves(0);

    const project = {
      nik: "BSP2100001",
      nama_project: "aplikasi mobile",
      tanggal_mulai: "2021-04-01",
      tanggal_selesai: "2021-05-01",
      customer: "Koperasi A",
      end_customer: "koperasi B",
      project_site: "kantorr",
      deskripsi_project: "membuat aplikasi mobile react",
      tech_info: "pakai react native",
      other_info: "tes",
      project_lang: "javascript, sql",
      project_framework: "react",
      project_app: "mobile",
      project_tool: "vscode",
      project_db: "mysql",
      project_os: "android",
      project_server: "ubuntu",
    };

    chai
      .request(app)
      .put("/api/cv/project/10")
      .set("auth-token", token)
      .send(project)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.be.have.status(200);
        sinon.restore();
        done();
      });
  });

  //update => 400
  it("should update project with error empty request body", () => {
    let req = {
      body: {},
      user: { nik: "" },
      params: { id_project: "" },
    };

    let res = {
      status: sinon.stub().returnsThis(),
      send: sinon.spy(),
    };

    projectController.update(req, res);
    expect(res.send.calledOnce).to.be.true;
    sinon.assert.calledWith(res.status, 400);
    sinon.assert.calledWith(res.send, { message: "Content can not be empty!" });
    sinon.restore();
  });

  //update => 401
  it("should update project with parameter id_project, but no token", (done) => {
    const project = {
      nik: "BSP2100001",
      nama_project: "aplikasi mobile",
      tanggal_mulai: "2021-04-01",
      tanggal_selesai: "2021-05-01",
      customer: "Koperasi A",
      end_customer: "koperasi B",
      project_site: "kantorr",
      deskripsi_project: "membuat aplikasi mobile react",
      tech_info: "pakai react native",
      other_info: "tes",
      project_lang: "javascript, sql",
      project_framework: "react",
      project_app: "mobile",
      project_tool: "vscode",
      project_db: "mysql",
      project_os: "android",
      project_server: "ubuntu",
    };

    chai
      .request(app)
      .put("/api/cv/project/10")
      .send(project)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.be.have.status(401);
        done();
      });
  });

  //update => 500
  it("should return error when update project data with parameter id_project", (done) => {
    sinon.stub(models.tb_project, "update").rejects({ message: "" });

    const project = {
      nik: "BSP2100001",
      nama_project: "aplikasi mobile",
      tanggal_mulai: "2021-04-01",
      tanggal_selesai: "2021-05-01",
      customer: "Koperasi A",
      end_customer: "koperasi B",
      project_site: "kantorr",
      deskripsi_project: "membuat aplikasi mobile react",
      tech_info: "pakai react native",
      other_info: "tes",
      project_lang: "javascript, sql",
      project_framework: "react",
      project_app: "mobile",
      project_tool: "vscode",
      project_db: "mysql",
      project_os: "android",
      project_server: "ubuntu",
    };

    chai
      .request(app)
      .put("/api/cv/project/10")
      .set("auth-token", token)
      .send(project)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.be.have.status(500);
        sinon.restore();
        done();
      });
  });

  //delete => 200 (success)
  it("should delete one project data with parameter id_project", (done) => {
    sinon.stub(models.tb_project, "destroy").resolves(1);

    chai
      .request(app)
      .delete("/api/cv/project/11")
      .set("auth-token", token)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.be.have.status(200);
        sinon.restore();
        done();
      });
  });

  //delete => 200 (failed)
  it("should delete one project data with parameter id_project", (done) => {
    sinon.stub(models.tb_project, "destroy").resolves(0);

    chai
      .request(app)
      .delete("/api/cv/project/11")
      .set("auth-token", token)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.be.have.status(200);
        sinon.restore();
        done();
      });
  });

  //delete => 401
  it("should delete one project data with parameter id_project, but no token", (done) => {
    chai
      .request(app)
      .delete("/api/cv/project/11")
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.be.have.status(401);
        done();
      });
  });

  //delete => 500
  it("should return error when delete project data with parameter id_project", (done) => {
    sinon.stub(models.tb_project, "destroy").rejects({ message: "" });

    chai
      .request(app)
      .delete("/api/cv/project/99")
      .set("auth-token", token)
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.be.have.status(500);
        sinon.restore();
        done();
      });
  });

  describe("Project Role API", () => {
    //get all roles project => 200
    it("should get all roles from specific project with parameter id_project", (done) => {
      chai
        .request(app)
        .get("/api/cv/project/roles/1")
        .set("auth-token", token)
        .end((err, res) => {
          expect(err).to.be.null;
          expect(res).to.have.status(200);
          done();
        });
    });

    //get all roles project => 500
    it("should get all roles from specific project with parameter id_project", (done) => {
      sinon.stub(models.tb_project_role, "findAll").rejects({ message: "" });

      chai
        .request(app)
        .get("/api/cv/project/roles/1")
        .set("auth-token", token)
        .end((err, res) => {
          expect(err).to.be.null;
          expect(res).to.have.status(500);
          sinon.restore();
          done();
        });
    });

    //get one role => 200
    it("should get one of several roles from specific project, with parameter id_pr", (done) => {
      chai
        .request(app)
        .get("/api/cv/project/role/1")
        .set("auth-token", token)
        .end((err, res) => {
          expect(err).to.be.null;
          expect(res).to.be.have.status(200);
          done();
        });
    });

    //get one role => 401
    it("should get one of several roles from specific project, with parameter id_pr, but no token", (done) => {
      chai
        .request(app)
        .get("/api/cv/project/role/1")
        .end((err, res) => {
          expect(err).to.be.null;
          expect(res).to.be.have.status(401);
          done();
        });
    });

    //get one role => 500
    it("should return error retrieve one of several roles from specific project, with parameter id_pr", (done) => {
      sinon.stub(models.tb_project_role, "findByPk").rejects({ message: "" });

      chai
        .request(app)
        .get("/api/cv/project/role/1")
        .set("auth-token", token)
        .end((err, res) => {
          console.log(res.body);
          expect(err).to.be.null;
          expect(res).to.be.have.status(500);
          sinon.restore();
          done();
        });
    });

    //post role => 200
    it("should post data role for the project, with parameter id_project", (done) => {
      const pr = {
        id_project: 1,
        role: "RL001",
      };

      chai
        .request(app)
        .post("/api/cv/project/role/1")
        .set("auth-token", token)
        .send(pr)
        .end((err, res) => {
          expect(err).to.be.null;
          expect(res).to.be.have.status(200);
          done();
        });
    });

    //post role => 400
    it("should post data role for the project with error empty request body, with parameter id_project", () => {
      let req = {
        user: { nik: "" },
        body: {},
        params: { id_project: "" },
      };

      let res = {
        status: sinon.stub().returnsThis(),
        send: sinon.spy(),
      };

      projectController.createRole(req, res);
      expect(res.send.calledOnce).to.be.true;
      sinon.assert.calledWith(res.status, 400);
      sinon.assert.calledWith(res.send, {
        message: "Content can not be empty!",
      });
    });

    //post role => 401
    it("should post data role for the project, with parameter id_project, but no token", (done) => {
      const pr = {
        id_project: 1,
        role: "RL001",
      };

      chai
        .request(app)
        .post("/api/cv/project/role/1")
        .send(pr)
        .end((err, res) => {
          expect(err).to.be.null;
          expect(res).to.be.have.status(401);
          done();
        });
    });

    //post role => 500
    it("should return error when post project role data, with parameter id_project", (done) => {
      sinon.stub(models.tb_project_role, "create").rejects({ message: "" });

      const pr = {
        id_project: 1,
        role: "RL001",
      };

      chai
        .request(app)
        .post("/api/cv/project/role/1")
        .set("auth-token", token)
        .send(pr)
        .end((err, res) => {
          expect(err).to.be.null;
          expect(res).to.be.have.status(500);
          sinon.restore();
          done();
        });
    });

    //delete role => 200 (success)
    it("should delete one of several roles from specific project, with parameter id_pr", (done) => {
      sinon.stub(models.tb_project_role, "destroy").resolves(1);

      chai
        .request(app)
        .delete("/api/cv/project/role/14")
        .set("auth-token", token)
        .end((err, res) => {
          expect(err).to.be.null;
          expect(res).to.be.have.status(200);
          sinon.restore();
          done();
        });
    });

    //delete role => 200 (failed)
    it("should delete one of several roles from specific project, with parameter id_pr", (done) => {
      sinon.stub(models.tb_project_role, "destroy").resolves(0);

      chai
        .request(app)
        .delete("/api/cv/project/role/14")
        .set("auth-token", token)
        .end((err, res) => {
          expect(err).to.be.null;
          expect(res).to.be.have.status(200);
          sinon.restore();
          done();
        });
    });

    //delete role => 401
    it("should delete one of several roles from specific project, with parameter id_pr, but no token", (done) => {
      chai
        .request(app)
        .delete("/api/cv/project/role/14")
        .end((err, res) => {
          expect(err).to.be.null;
          expect(res).to.be.have.status(401);
          done();
        });
    });

    //delete role => 500
    it("should return error when delete project role data with parameter id_pr", (done) => {
      sinon.stub(models.tb_project_role, "destroy").rejects({ message: "" });

      chai
        .request(app)
        .delete("/api/cv/project/role/99")
        .set("auth-token", token)
        .end((err, res) => {
          expect(err).to.be.null;
          expect(res).to.be.have.status(500);
          sinon.restore();
          done();
        });
    });
  });
});
