const scanner = require('sonarqube-scanner');

scanner(
    {
      serverUrl : 'http://localhost:9000',
      token: 'd84b3698cf3399c916a53ccf3c4ff6f89968dc4d',
      options: {
        'sonar.projectName': 'Server CV',
        'sonar.projectDescription': 'Aplikasi backend untuk resume app',
        'sonar.sources': '.',
        'sonar.inclusions' : 'controllers/*', // Entry point of your code,
        'sonar.javascript.lcov.reportPaths': 'coverage/lcov.info'
      }
    },
    () => process.exit()
  )