const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('tb_personal', {
    nik: {
      type: DataTypes.CHAR(10),
      allowNull: false,
      primaryKey: true
    },
    nama_depan: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    nama_belakang: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    tempat_lahir: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    tanggal_lahir: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    agama: {
      type: DataTypes.TINYINT,
      allowNull: true,
      references: {
        model: 'tb_m_agama',
        key: 'kd_agama'
      }
    },
    jenis_kelamin: {
      type: DataTypes.ENUM('L','P'),
      allowNull: true
    },
    kesehatan: {
      type: DataTypes.ENUM('Good','Medium','Bad'),
      allowNull: true
    },
    foto: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    role_terakhir: {
      type: DataTypes.CHAR(5),
      allowNull: true,
      references: {
        model: 'tb_m_daftar_pekerjaan',
        key: 'kd_jabatan'
      }
    },
    list_profile: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    list_ptech: {
      type: DataTypes.TEXT,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'tb_personal',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "nik" },
        ]
      },
      {
        name: "agama",
        using: "BTREE",
        fields: [
          { name: "agama" },
        ]
      },
      {
        name: "role_terakhir",
        using: "BTREE",
        fields: [
          { name: "role_terakhir" },
        ]
      },
    ]
  });
};
