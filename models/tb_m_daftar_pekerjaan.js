const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('tb_m_daftar_pekerjaan', {
    kd_jabatan: {
      type: DataTypes.CHAR(5),
      allowNull: false,
      primaryKey: true
    },
    nama_jabatan: {
      type: DataTypes.STRING(50),
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'tb_m_daftar_pekerjaan',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "kd_jabatan" },
        ]
      },
    ]
  });
};
