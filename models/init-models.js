const Sequelize = require("sequelize");
const DataTypes = require("sequelize").DataTypes;
const dbConfig = require("../config/db.config");

var _tb_m_agama = require("./tb_m_agama");
var _tb_m_daftar_pekerjaan = require("./tb_m_daftar_pekerjaan");
var _tb_m_gelar = require("./tb_m_gelar");
var _tb_p_kursus = require("./tb_p_kursus");
var _tb_p_pendidikan = require("./tb_p_pendidikan");
var _tb_p_pengalaman_bahasa = require("./tb_p_pengalaman_bahasa");
var _tb_p_pengalaman_kerja = require("./tb_p_pengalaman_kerja");
var _tb_p_pengalaman_kerja_role = require("./tb_p_pengalaman_kerja_role");
var _tb_p_prestasi = require("./tb_p_prestasi");
var _tb_p_prestasi_detail = require("./tb_p_prestasi_detail");
var _tb_p_profile = require("./tb_p_profile");
var _tb_p_profile_tech = require("./tb_p_profile_tech");
var _tb_personal = require("./tb_personal");
var _tb_project = require("./tb_project");
var _tb_project_app = require("./tb_project_app");
var _tb_project_db = require("./tb_project_db");
var _tb_project_framework = require("./tb_project_framework");
var _tb_project_lang = require("./tb_project_lang");
var _tb_project_os = require("./tb_project_os");
var _tb_project_role = require("./tb_project_role");
var _tb_project_server = require("./tb_project_server");
var _tb_project_tool = require("./tb_project_tool");
var _tb_user = require("./tb_user");
var _tb_user_seq = require("./tb_user_seq");

const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, {
  host: dbConfig.HOST,
  dialect: dbConfig.dialect,
  operatorsAliases: 0,
  logging: false
});

function initModels(sequelize) {
  var tb_m_agama = _tb_m_agama(sequelize, DataTypes);
  var tb_m_daftar_pekerjaan = _tb_m_daftar_pekerjaan(sequelize, DataTypes);
  var tb_m_gelar = _tb_m_gelar(sequelize, DataTypes);
  var tb_p_kursus = _tb_p_kursus(sequelize, DataTypes);
  var tb_p_pendidikan = _tb_p_pendidikan(sequelize, DataTypes);
  var tb_p_pengalaman_bahasa = _tb_p_pengalaman_bahasa(sequelize, DataTypes);
  var tb_p_pengalaman_kerja = _tb_p_pengalaman_kerja(sequelize, DataTypes);
  var tb_p_pengalaman_kerja_role = _tb_p_pengalaman_kerja_role(sequelize, DataTypes);
  var tb_p_prestasi = _tb_p_prestasi(sequelize, DataTypes);
  var tb_p_prestasi_detail = _tb_p_prestasi_detail(sequelize, DataTypes);
  var tb_p_profile = _tb_p_profile(sequelize, DataTypes);
  var tb_p_profile_tech = _tb_p_profile_tech(sequelize, DataTypes);
  var tb_personal = _tb_personal(sequelize, DataTypes);
  var tb_project = _tb_project(sequelize, DataTypes);
  var tb_project_app = _tb_project_app(sequelize, DataTypes);
  var tb_project_db = _tb_project_db(sequelize, DataTypes);
  var tb_project_framework = _tb_project_framework(sequelize, DataTypes);
  var tb_project_lang = _tb_project_lang(sequelize, DataTypes);
  var tb_project_os = _tb_project_os(sequelize, DataTypes);
  var tb_project_role = _tb_project_role(sequelize, DataTypes);
  var tb_project_server = _tb_project_server(sequelize, DataTypes);
  var tb_project_tool = _tb_project_tool(sequelize, DataTypes);
  var tb_user = _tb_user(sequelize, DataTypes);
  var tb_user_seq = _tb_user_seq(sequelize, DataTypes);

  tb_personal.belongsTo(tb_m_agama, { as: "agama_tb_m_agama", foreignKey: "agama"});
  tb_m_agama.hasMany(tb_personal, { as: "tb_personals", foreignKey: "agama"});
  tb_p_kursus.belongsTo(tb_m_daftar_pekerjaan, { as: "role_tb_m_daftar_pekerjaan", foreignKey: "role"});
  tb_m_daftar_pekerjaan.hasMany(tb_p_kursus, { as: "tb_p_kursus", foreignKey: "role"});
  tb_p_pengalaman_kerja_role.belongsTo(tb_m_daftar_pekerjaan, { as: "kd_jabatan_tb_m_daftar_pekerjaan", foreignKey: "kd_jabatan"});
  tb_m_daftar_pekerjaan.hasMany(tb_p_pengalaman_kerja_role, { as: "tb_p_pengalaman_kerja_roles", foreignKey: "kd_jabatan"});
  tb_p_prestasi.belongsTo(tb_m_daftar_pekerjaan, { as: "kd_jabatan_tb_m_daftar_pekerjaan", foreignKey: "kd_jabatan"});
  tb_m_daftar_pekerjaan.hasMany(tb_p_prestasi, { as: "tb_p_prestasis", foreignKey: "kd_jabatan"});
  tb_personal.belongsTo(tb_m_daftar_pekerjaan, { as: "role_terakhir_tb_m_daftar_pekerjaan", foreignKey: "role_terakhir"});
  tb_m_daftar_pekerjaan.hasMany(tb_personal, { as: "tb_personals", foreignKey: "role_terakhir"});
  tb_project_role.belongsTo(tb_m_daftar_pekerjaan, { as: "role_tb_m_daftar_pekerjaan", foreignKey: "role"});
  tb_m_daftar_pekerjaan.hasMany(tb_project_role, { as: "tb_project_roles", foreignKey: "role"});
  tb_p_pendidikan.belongsTo(tb_m_gelar, { as: "tingkat_pendidikan_tb_m_gelar", foreignKey: "tingkat_pendidikan"});
  tb_m_gelar.hasMany(tb_p_pendidikan, { as: "tb_p_pendidikans", foreignKey: "tingkat_pendidikan"});
  tb_p_pengalaman_kerja_role.belongsTo(tb_p_pengalaman_kerja, { as: "id_pengalaman_kerja_tb_p_pengalaman_kerja", foreignKey: "id_pengalaman_kerja"});
  tb_p_pengalaman_kerja.hasMany(tb_p_pengalaman_kerja_role, { as: "tb_p_pengalaman_kerja_roles", foreignKey: "id_pengalaman_kerja"});
  tb_p_prestasi_detail.belongsTo(tb_p_prestasi, { as: "id_prestasi_tb_p_prestasi", foreignKey: "id_prestasi"});
  tb_p_prestasi.hasMany(tb_p_prestasi_detail, { as: "tb_p_prestasi_details", foreignKey: "id_prestasi"});
  tb_p_kursus.belongsTo(tb_personal, { as: "nik_tb_personal", foreignKey: "nik"});
  tb_personal.hasMany(tb_p_kursus, { as: "tb_p_kursus", foreignKey: "nik"});
  tb_p_pendidikan.belongsTo(tb_personal, { as: "nik_tb_personal", foreignKey: "nik"});
  tb_personal.hasMany(tb_p_pendidikan, { as: "tb_p_pendidikans", foreignKey: "nik"});
  tb_p_pengalaman_bahasa.belongsTo(tb_personal, { as: "nik_tb_personal", foreignKey: "nik"});
  tb_personal.hasMany(tb_p_pengalaman_bahasa, { as: "tb_p_pengalaman_bahasas", foreignKey: "nik"});
  tb_p_pengalaman_kerja.belongsTo(tb_personal, { as: "nik_tb_personal", foreignKey: "nik"});
  tb_personal.hasMany(tb_p_pengalaman_kerja, { as: "tb_p_pengalaman_kerjas", foreignKey: "nik"});
  tb_p_prestasi.belongsTo(tb_personal, { as: "nik_tb_personal", foreignKey: "nik"});
  tb_personal.hasMany(tb_p_prestasi, { as: "tb_p_prestasis", foreignKey: "nik"});
  tb_p_profile.belongsTo(tb_personal, { as: "nik_tb_personal", foreignKey: "nik"});
  tb_personal.hasMany(tb_p_profile, { as: "tb_p_profiles", foreignKey: "nik"});
  tb_p_profile_tech.belongsTo(tb_personal, { as: "nik_tb_personal", foreignKey: "nik"});
  tb_personal.hasMany(tb_p_profile_tech, { as: "tb_p_profile_teches", foreignKey: "nik"});
  tb_project.belongsTo(tb_personal, { as: "nik_tb_personal", foreignKey: "nik"});
  tb_personal.hasMany(tb_project, { as: "tb_projects", foreignKey: "nik"});
  tb_project_app.belongsTo(tb_project, { as: "id_project_tb_project", foreignKey: "id_project"});
  tb_project.hasMany(tb_project_app, { as: "tb_project_apps", foreignKey: "id_project"});
  tb_project_db.belongsTo(tb_project, { as: "id_project_tb_project", foreignKey: "id_project"});
  tb_project.hasMany(tb_project_db, { as: "tb_project_dbs", foreignKey: "id_project"});
  tb_project_framework.belongsTo(tb_project, { as: "id_project_tb_project", foreignKey: "id_project"});
  tb_project.hasMany(tb_project_framework, { as: "tb_project_frameworks", foreignKey: "id_project"});
  tb_project_lang.belongsTo(tb_project, { as: "id_project_tb_project", foreignKey: "id_project"});
  tb_project.hasMany(tb_project_lang, { as: "tb_project_langs", foreignKey: "id_project"});
  tb_project_os.belongsTo(tb_project, { as: "id_project_tb_project", foreignKey: "id_project"});
  tb_project.hasMany(tb_project_os, { as: "tb_project_os", foreignKey: "id_project"});
  tb_project_role.belongsTo(tb_project, { as: "id_project_tb_project", foreignKey: "id_project"});
  tb_project.hasMany(tb_project_role, { as: "tb_project_roles", foreignKey: "id_project"});
  tb_project_server.belongsTo(tb_project, { as: "id_project_tb_project", foreignKey: "id_project"});
  tb_project.hasMany(tb_project_server, { as: "tb_project_servers", foreignKey: "id_project"});
  tb_project_tool.belongsTo(tb_project, { as: "id_project_tb_project", foreignKey: "id_project"});
  tb_project.hasMany(tb_project_tool, { as: "tb_project_tools", foreignKey: "id_project"});

  return {
    tb_m_agama,
    tb_m_daftar_pekerjaan,
    tb_m_gelar,
    tb_p_kursus,
    tb_p_pendidikan,
    tb_p_pengalaman_bahasa,
    tb_p_pengalaman_kerja,
    tb_p_pengalaman_kerja_role,
    tb_p_prestasi,
    tb_p_prestasi_detail,
    tb_p_profile,
    tb_p_profile_tech,
    tb_personal,
    tb_project,
    tb_project_app,
    tb_project_db,
    tb_project_framework,
    tb_project_lang,
    tb_project_os,
    tb_project_role,
    tb_project_server,
    tb_project_tool,
    tb_user,
    tb_user_seq,
  };
}
module.exports = initModels(sequelize);
module.exports.initModels = initModels(sequelize);
module.exports.default = initModels(sequelize);
