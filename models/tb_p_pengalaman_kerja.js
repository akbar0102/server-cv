const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('tb_p_pengalaman_kerja', {
    id_pengalaman_kerja: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    nik: {
      type: DataTypes.CHAR(10),
      allowNull: false,
      references: {
        model: 'tb_personal',
        key: 'nik'
      }
    },
    perusahaan: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    tanggal_masuk: {
      type: DataTypes.DATEONLY,
      allowNull: false
    },
    tanggal_keluar: {
      type: DataTypes.DATEONLY,
      allowNull: false
    },
    status_pegawai: {
      type: DataTypes.STRING(50),
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'tb_p_pengalaman_kerja',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id_pengalaman_kerja" },
        ]
      },
      {
        name: "nik",
        using: "BTREE",
        fields: [
          { name: "nik" },
        ]
      },
    ]
  });
};
