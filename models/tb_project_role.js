const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('tb_project_role', {
    id_pr: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    id_project: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'tb_project',
        key: 'id_project'
      }
    },
    role: {
      type: DataTypes.CHAR(5),
      allowNull: false,
      references: {
        model: 'tb_m_daftar_pekerjaan',
        key: 'kd_jabatan'
      }
    }
  }, {
    sequelize,
    tableName: 'tb_project_role',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id_pr" },
        ]
      },
      {
        name: "role",
        using: "BTREE",
        fields: [
          { name: "role" },
        ]
      },
      {
        name: "fk_project",
        using: "BTREE",
        fields: [
          { name: "id_project" },
        ]
      },
    ]
  });
};
