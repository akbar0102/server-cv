const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('tb_p_prestasi', {
    id_prestasi: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    nik: {
      type: DataTypes.CHAR(10),
      allowNull: false,
      references: {
        model: 'tb_personal',
        key: 'nik'
      }
    },
    kd_jabatan: {
      type: DataTypes.CHAR(5),
      allowNull: false,
      references: {
        model: 'tb_m_daftar_pekerjaan',
        key: 'kd_jabatan'
      }
    },
    detail: {
      type: DataTypes.TEXT,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'tb_p_prestasi',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id_prestasi" },
        ]
      },
      {
        name: "nik",
        using: "BTREE",
        fields: [
          { name: "nik" },
        ]
      },
      {
        name: "kd_jabatan",
        using: "BTREE",
        fields: [
          { name: "kd_jabatan" },
        ]
      },
    ]
  });
};
