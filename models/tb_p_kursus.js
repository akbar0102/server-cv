const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('tb_p_kursus', {
    id_kursus: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    nik: {
      type: DataTypes.CHAR(10),
      allowNull: false,
      references: {
        model: 'tb_personal',
        key: 'nik'
      }
    },
    judul_kursus: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    penyelenggara: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    tempat: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    tanggal_mulai: {
      type: DataTypes.DATEONLY,
      allowNull: false
    },
    tanggal_selesai: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    lama_kegiatan: {
      type: DataTypes.TINYINT,
      allowNull: false
    },
    sertifikat: {
      type: DataTypes.ENUM('Ya','Tidak'),
      allowNull: false
    },
    role: {
      type: DataTypes.CHAR(5),
      allowNull: true,
      references: {
        model: 'tb_m_daftar_pekerjaan',
        key: 'kd_jabatan'
      }
    }
  }, {
    sequelize,
    tableName: 'tb_p_kursus',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id_kursus" },
        ]
      },
      {
        name: "nik",
        using: "BTREE",
        fields: [
          { name: "nik" },
        ]
      },
      {
        name: "tb_p_kursus_role",
        using: "BTREE",
        fields: [
          { name: "role" },
        ]
      },
    ]
  });
};
