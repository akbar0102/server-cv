const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('tb_p_pengalaman_kerja_role', {
    id_role: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    id_pengalaman_kerja: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'tb_p_pengalaman_kerja',
        key: 'id_pengalaman_kerja'
      }
    },
    kd_jabatan: {
      type: DataTypes.CHAR(5),
      allowNull: false,
      references: {
        model: 'tb_m_daftar_pekerjaan',
        key: 'kd_jabatan'
      }
    }
  }, {
    sequelize,
    tableName: 'tb_p_pengalaman_kerja_role',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id_role" },
        ]
      },
      {
        name: "kd_jabatan",
        using: "BTREE",
        fields: [
          { name: "kd_jabatan" },
        ]
      },
      {
        name: "tb_p_pengalaman_kerja_role_ibfk_1",
        using: "BTREE",
        fields: [
          { name: "id_pengalaman_kerja" },
        ]
      },
    ]
  });
};
