const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('tb_project_framework', {
    id_framework: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    id_project: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'tb_project',
        key: 'id_project'
      }
    },
    framework: {
      type: DataTypes.STRING(20),
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'tb_project_framework',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id_framework" },
        ]
      },
      {
        name: "id_project",
        using: "BTREE",
        fields: [
          { name: "id_project" },
        ]
      },
    ]
  });
};
