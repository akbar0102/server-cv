const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('tb_p_prestasi_detail', {
    id_detail_prestasi: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    id_prestasi: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'tb_p_prestasi',
        key: 'id_prestasi'
      }
    },
    detail: {
      type: DataTypes.TEXT,
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'tb_p_prestasi_detail',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id_detail_prestasi" },
        ]
      },
      {
        name: "id_prestasi",
        using: "BTREE",
        fields: [
          { name: "id_prestasi" },
        ]
      },
    ]
  });
};
