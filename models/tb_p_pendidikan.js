const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('tb_p_pendidikan', {
    id_pendidikan: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    nik: {
      type: DataTypes.CHAR(10),
      allowNull: false,
      references: {
        model: 'tb_personal',
        key: 'nik'
      }
    },
    instansi: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    tingkat_pendidikan: {
      type: DataTypes.TINYINT,
      allowNull: false,
      references: {
        model: 'tb_m_gelar',
        key: 'kd_gelar'
      }
    },
    jurusan: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    tahun_masuk: {
      type: DataTypes.SMALLINT,
      allowNull: false
    },
    tahun_keluar: {
      type: DataTypes.SMALLINT,
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'tb_p_pendidikan',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id_pendidikan" },
        ]
      },
      {
        name: "nik",
        using: "BTREE",
        fields: [
          { name: "nik" },
        ]
      },
      {
        name: "tingkat_pendidikan",
        using: "BTREE",
        fields: [
          { name: "tingkat_pendidikan" },
        ]
      },
    ]
  });
};
