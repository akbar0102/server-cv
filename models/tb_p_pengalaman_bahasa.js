const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('tb_p_pengalaman_bahasa', {
    id_bahasa: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    nik: {
      type: DataTypes.CHAR(10),
      allowNull: false,
      references: {
        model: 'tb_personal',
        key: 'nik'
      }
    },
    bahasa: {
      type: DataTypes.STRING(20),
      allowNull: false
    },
    tingkat: {
      type: DataTypes.ENUM('Good','Medium','Low'),
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'tb_p_pengalaman_bahasa',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id_bahasa" },
        ]
      },
      {
        name: "nik",
        using: "BTREE",
        fields: [
          { name: "nik" },
        ]
      },
    ]
  });
};
