const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('tb_project', {
    id_project: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    nik: {
      type: DataTypes.CHAR(10),
      allowNull: false,
      references: {
        model: 'tb_personal',
        key: 'nik'
      }
    },
    nama_project: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    tanggal_mulai: {
      type: DataTypes.DATEONLY,
      allowNull: false
    },
    tanggal_selesai: {
      type: DataTypes.DATEONLY,
      allowNull: false
    },
    customer: {
      type: DataTypes.STRING(500),
      allowNull: false
    },
    end_customer: {
      type: DataTypes.STRING(500),
      allowNull: true
    },
    project_site: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    deskripsi_project: {
      type: DataTypes.STRING(500),
      allowNull: true
    },
    tech_info: {
      type: DataTypes.STRING(500),
      allowNull: true
    },
    other_info: {
      type: DataTypes.STRING(500),
      allowNull: true
    },
    project_lang: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    project_framework: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    project_app: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    project_tool: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    project_db: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    project_os: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    project_server: {
      type: DataTypes.STRING(255),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'tb_project',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id_project" },
        ]
      },
      {
        name: "nik",
        using: "BTREE",
        fields: [
          { name: "nik" },
        ]
      },
    ]
  });
};
