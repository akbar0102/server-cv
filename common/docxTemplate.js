const createReport = require('docx-templates').default;
const fs = require('fs');
const fetch = require('node-fetch');

const wordTemplate = function(res, data){
    return new Promise((resolve, reject) => {
        let timestamp = new Date().getTime();
        let url = `public/templates/output_${timestamp}.docx`;
        let template = fs.readFileSync('public/templates/resume.docx');
        
        createReport({
            template: template,
            output: url,
            data: data,
            additionalJsContext: {
                inject: async (url) => {
                    const resp = await fetch(
                        `http://localhost:3000/${url}`
                      );
                      const buffer = resp.arrayBuffer
                        ? await resp.arrayBuffer()
                        : await resp.buffer();
                      return { width: 4, height: 4, data: buffer, extension: '.png' };                  
                  }
              }
        }).then((res)=>{
            fs.writeFileSync(url, res);
            resolve({data: url.replace('public/', ''), status: 'Success'}); 
        }).catch((err)=>{
            reject(err);
        })
    });
}

module.exports = wordTemplate;