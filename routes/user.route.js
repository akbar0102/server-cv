const router = require("express").Router();
const user = require("../controllers/user.controller");

router.post("/cv/register", user.create);
router.post("/cv/login", user.getOne);

module.exports = router;
