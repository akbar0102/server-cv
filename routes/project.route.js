const router = require("express").Router();
const verify = require("../middlewares/verifyToken");
const project = require("../controllers/project.controller");

router.post("/cv/project", verify, project.create);
router.get("/cv/project", verify, project.find);
router.get("/cv/project/:id_project", verify, project.findOne);
router.put("/cv/project/:id_project", verify, project.update);
router.delete("/cv/project/:id_project", verify, project.delete);
router.get("/cv/project/roles/:id_project", verify, project.findRole);

//project role
router.post("/cv/project/role/:id_project", verify, project.createRole);
router.get("/cv/project/role/:id_pr", verify, project.findOneRole);
router.delete("/cv/project/role/:id_pr", verify, project.deleteRole);
// router.put("/cv/project/role/:id_pr", verify, project.updateRole);

module.exports = router;
