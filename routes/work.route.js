const router = require("express").Router();
const verify = require("../middlewares/verifyToken");
const work = require("../controllers/work.controller");

router.post("/cv/work", verify, work.create);
router.get("/cv/work", verify, work.find);
router.get("/cv/work/:id_pengalaman_kerja", verify, work.findOne);
router.put("/cv/work/:id_pengalaman_kerja", verify, work.update);
router.delete("/cv/work/:id_pengalaman_kerja", verify, work.delete);
router.get("/cv/work/roles/:id_pengalaman_kerja", verify, work.findRole);

//work role
router.post("/cv/work/role/:id_pengalaman_kerja", verify, work.createRole);
router.get("/cv/work/role/:id_role", verify, work.findOneRole);
router.delete("/cv/work/role/:id_role", verify, work.deleteRole);
//outer.put("/cv/work/role/:id_role", verify, work.updateRole);

module.exports = router;
