const router = require("express").Router();
const verify = require("../middlewares/verifyToken");
const education = require("../controllers/education.controller");

router.post("/cv/education", verify, education.create);
router.get("/cv/education", verify, education.find);
router.get("/cv/education/:id_pendidikan", verify, education.findOne);
router.put("/cv/education/:id_pendidikan", verify, education.update);
router.delete("/cv/education/:id_pendidikan", verify, education.delete);

module.exports = router;
