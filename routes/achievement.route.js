const router = require("express").Router();
const verify = require("../middlewares/verifyToken");
const achievement = require("../controllers/achievement.controller");

router.post("/cv/achievement", verify, achievement.create);
router.get("/cv/achievement", verify, achievement.find);
router.get("/cv/achievement/:id_prestasi", verify, achievement.findOne);
router.put("/cv/achievement/:id_prestasi", verify, achievement.update);
router.delete("/cv/achievement/:id_prestasi", verify, achievement.delete);

module.exports = router;
