const router = require("express").Router();
const verify = require("../middlewares/verifyToken");
const { upload } = require("../middlewares/multer");
const personal = require("../controllers/personal.controller");

router.get("/cv/person", verify, personal.findOne);
router.put("/cv/person", verify, upload, personal.update);
router.get("/cv/person/generate", verify, personal.generateDocx);

router.post("/cv/person/language", verify, personal.createBahasa);
router.get("/cv/person/language", verify, personal.readBahasa);
router.get("/cv/person/language/:id_bahasa", verify, personal.readOneBahasa);
router.put("/cv/person/language/:id_bahasa", verify, personal.updateBahasa);
router.delete("/cv/person/language/:id_bahasa", verify, personal.deleteBahasa);

module.exports = router;
