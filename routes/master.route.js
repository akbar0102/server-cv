const router = require("express").Router();
const verify = require("../middlewares/verifyToken");
const job = require('../controllers/job.controller');
const degree = require('../controllers/degree.controller');
const religion = require("../controllers/religion.controller");
const personal = require("../controllers/personal.controller");

router.get('/cv/master/person', verify, personal.findALl);

router.get('/cv/master/degree', verify, degree.findAll);
router.get('/cv/master/degree/:kd_gelar', verify, degree.findOne);
router.post('/cv/master/degree', verify, degree.create);
router.put('/cv/master/degree/:kd_gelar', verify, degree.update);
// router.delete('/cv/master/degree/:kd_gelar', verify, degree.delete);

router.get('/cv/master/religion', verify, religion.findAll);
router.get('/cv/master/religion/:kd_agama', verify, religion.findOne);
router.post('/cv/master/religion', verify, religion.create);
router.put('/cv/master/religion/:kd_agama', verify, religion.update);
// router.delete('/cv/master/religion/:kd_agama', verify, religion.delete);

router.get('/cv/master/job', verify, job.findAll);
router.get('/cv/master/job/:kd_jabatan', verify, job.findOne);
router.post('/cv/master/job', verify, job.create);
router.put('/cv/master/job/:kd_jabatan', verify, job.update);
// router.delete('/cv/master/job/:kd_jabatan', verify, job.delete);

module.exports = router;