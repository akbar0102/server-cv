const router = require("express").Router();
const verify = require("../middlewares/verifyToken");
const course = require("../controllers/course.controller");

router.post("/cv/course", verify, course.create);
router.get("/cv/course", verify, course.find);
router.get("/cv/course/:id_kursus", verify, course.findOne);
router.put("/cv/course/:id_kursus", verify, course.update);
router.delete("/cv/course/:id_kursus", verify, course.delete);

module.exports = router;
