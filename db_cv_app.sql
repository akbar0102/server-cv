-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 01 Apr 2021 pada 04.06
-- Versi server: 10.4.13-MariaDB
-- Versi PHP: 7.4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_cv_app`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_m_agama`
--

CREATE TABLE `tb_m_agama` (
  `kd_agama` tinyint(4) NOT NULL,
  `nama_agama` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_m_agama`
--

INSERT INTO `tb_m_agama` (`kd_agama`, `nama_agama`) VALUES
(1, 'Islam'),
(2, 'Kristen Katolik'),
(3, 'Kristen Protestan');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_m_daftar_pekerjaan`
--

CREATE TABLE `tb_m_daftar_pekerjaan` (
  `kd_jabatan` char(5) NOT NULL,
  `nama_jabatan` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_m_daftar_pekerjaan`
--

INSERT INTO `tb_m_daftar_pekerjaan` (`kd_jabatan`, `nama_jabatan`) VALUES
('RL001', 'Coordinator Bootcamp'),
('RL002', 'Facilitator'),
('RL003', 'Lead Programmer'),
('RL004', 'Programmer'),
('RL005', 'Tester'),
('RL006', 'Senior Programmer');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_m_gelar`
--

CREATE TABLE `tb_m_gelar` (
  `kd_gelar` tinyint(4) NOT NULL,
  `nama_gelar` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_m_gelar`
--

INSERT INTO `tb_m_gelar` (`kd_gelar`, `nama_gelar`) VALUES
(1, 'SD'),
(2, 'SMP'),
(3, 'SMA'),
(4, 'SMK'),
(5, 'Diploma'),
(6, 'Sarjana'),
(7, 'Magister');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_personal`
--

CREATE TABLE `tb_personal` (
  `nik` char(10) NOT NULL,
  `nama_depan` varchar(50) DEFAULT NULL,
  `nama_belakang` varchar(50) DEFAULT NULL,
  `tempat_lahir` varchar(50) DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `agama` tinyint(4) DEFAULT NULL,
  `jenis_kelamin` enum('L','P') DEFAULT NULL,
  `kesehatan` enum('Good','Medium','Bad') DEFAULT NULL,
  `foto` varchar(100) DEFAULT NULL,
  `role_terakhir` char(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_personal`
--

INSERT INTO `tb_personal` (`nik`, `nama_depan`, `nama_belakang`, `tempat_lahir`, `tanggal_lahir`, `agama`, `jenis_kelamin`, `kesehatan`, `foto`, `role_terakhir`) VALUES
('BSP2100001', 'Yuda', 'Permana', 'Bandung', '1989-03-05', 1, 'L', 'Good', 'foto', 'RL006'),
('BSP2100003', 'Risda', 'Zaidah', 'Bandung', '1995-04-14', 1, 'P', 'Good', 'foto', 'RL004'),
('BSP2100005', 'Akbar', 'Hidayatuloh', 'Bandung', '1996-02-01', 1, 'L', 'Good', 'images/1617147358457.jpg', 'RL004');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_project`
--

CREATE TABLE `tb_project` (
  `id_project` int(11) NOT NULL,
  `nik` char(10) NOT NULL,
  `nama_project` varchar(50) NOT NULL,
  `tanggal_mulai` date NOT NULL,
  `tanggal_selesai` date NOT NULL,
  `customer` varchar(500) NOT NULL,
  `end_customer` varchar(500) DEFAULT NULL,
  `project_site` varchar(100) DEFAULT NULL,
  `deskripsi_project` varchar(500) DEFAULT NULL,
  `tech_info` varchar(500) DEFAULT NULL,
  `other_info` varchar(500) DEFAULT NULL,
  `project_lang` varchar(255) DEFAULT NULL,
  `project_framework` varchar(255) DEFAULT NULL,
  `project_app` varchar(255) DEFAULT NULL,
  `project_tool` varchar(255) DEFAULT NULL,
  `project_db` varchar(255) DEFAULT NULL,
  `project_os` varchar(255) DEFAULT NULL,
  `project_server` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_project`
--

INSERT INTO `tb_project` (`id_project`, `nik`, `nama_project`, `tanggal_mulai`, `tanggal_selesai`, `customer`, `end_customer`, `project_site`, `deskripsi_project`, `tech_info`, `other_info`, `project_lang`, `project_framework`, `project_app`, `project_tool`, `project_db`, `project_os`, `project_server`) VALUES
(1, 'BSP2100001', 'Bootcamp 2021', '2020-01-01', '2021-03-30', 'New Member PT BSP (Bee Solution Partners) ', NULL, NULL, 'Traine new programmer, to learn programming in Java, framework Struts, Spring, terasoluna, etc ', 'Sharing Standardization Coding,  Coding and Testing application', NULL, 'Java, Javascript', 'NodeJs, VueJs, Spring', NULL, 'Eclipse', 'Mysql, MongoDB', 'Windows', 'Apache, Tomcat'),
(2, 'BSP2100001', 'Indigo', '2020-09-01', '2020-12-31', 'PT Telkom Indonesia', NULL, NULL, 'Create portal and admin web application, for manage data contestant Indigo program from PT Telkom Indonesia', 'Coding and Testing Application', 'CI/CD use Jenkins, source management use Gitlab, Project management use Trello', 'Java, SQL', 'NodeJs, VueJs, NPM', NULL, 'VSCode, DbForge', 'Mysql', 'Redhat', 'Apache');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_project_app`
--

CREATE TABLE `tb_project_app` (
  `id_app` int(11) NOT NULL,
  `id_project` int(11) NOT NULL,
  `tipe` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_project_db`
--

CREATE TABLE `tb_project_db` (
  `id_db` int(11) NOT NULL,
  `id_project` int(11) NOT NULL,
  `db` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_project_db`
--

INSERT INTO `tb_project_db` (`id_db`, `id_project`, `db`) VALUES
(1, 1, 'Mysql');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_project_framework`
--

CREATE TABLE `tb_project_framework` (
  `id_framework` int(11) NOT NULL,
  `id_project` int(11) NOT NULL,
  `framework` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_project_framework`
--

INSERT INTO `tb_project_framework` (`id_framework`, `id_project`, `framework`) VALUES
(1, 1, 'NodeJs'),
(2, 1, 'VueJs'),
(3, 1, 'Spring');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_project_lang`
--

CREATE TABLE `tb_project_lang` (
  `id_lang` int(11) NOT NULL,
  `id_project` int(11) NOT NULL,
  `bahasa` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_project_lang`
--

INSERT INTO `tb_project_lang` (`id_lang`, `id_project`, `bahasa`) VALUES
(1, 1, 'Java'),
(2, 1, 'Javascript');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_project_os`
--

CREATE TABLE `tb_project_os` (
  `id_os` int(11) NOT NULL,
  `id_project` int(11) NOT NULL,
  `os` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_project_os`
--

INSERT INTO `tb_project_os` (`id_os`, `id_project`, `os`) VALUES
(1, 1, 'Windows');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_project_role`
--

CREATE TABLE `tb_project_role` (
  `id_pr` int(11) NOT NULL,
  `id_project` int(11) NOT NULL,
  `role` char(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_project_role`
--

INSERT INTO `tb_project_role` (`id_pr`, `id_project`, `role`) VALUES
(1, 1, 'RL002'),
(2, 2, 'RL004');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_project_server`
--

CREATE TABLE `tb_project_server` (
  `id_server` int(11) NOT NULL,
  `id_project` int(11) NOT NULL,
  `server` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_project_server`
--

INSERT INTO `tb_project_server` (`id_server`, `id_project`, `server`) VALUES
(1, 1, 'Tomcat'),
(2, 1, 'Apache');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_project_tool`
--

CREATE TABLE `tb_project_tool` (
  `id_tool` int(11) NOT NULL,
  `id_project` int(11) NOT NULL,
  `tool` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_project_tool`
--

INSERT INTO `tb_project_tool` (`id_tool`, `id_project`, `tool`) VALUES
(1, 1, 'Eclipse');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_p_kursus`
--

CREATE TABLE `tb_p_kursus` (
  `id_kursus` int(11) NOT NULL,
  `nik` char(10) NOT NULL,
  `judul_kursus` varchar(50) NOT NULL,
  `penyelenggara` varchar(100) NOT NULL,
  `tempat` varchar(50) NOT NULL,
  `tanggal_mulai` date NOT NULL,
  `tanggal_selesai` date DEFAULT NULL,
  `lama_kegiatan` tinyint(4) NOT NULL,
  `sertifikat` enum('Ya','Tidak') NOT NULL,
  `role` char(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_p_kursus`
--

INSERT INTO `tb_p_kursus` (`id_kursus`, `nik`, `judul_kursus`, `penyelenggara`, `tempat`, `tanggal_mulai`, `tanggal_selesai`, `lama_kegiatan`, `sertifikat`, `role`) VALUES
(3, 'BSP2100001', 'Microsoft Shared Point 2012', 'Politeknik Negeri Bandung (POLBAN)', 'POLBAN', '2013-04-04', '2013-04-05', 2, 'Tidak', NULL),
(12, 'BSP2100001', 'Bootcamp ASP.NET MVC 3', 'PT Bee Solution Partners ', 'BSP', '2013-04-01', '2013-04-07', 7, 'Ya', 'RL004');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_p_pendidikan`
--

CREATE TABLE `tb_p_pendidikan` (
  `id_pendidikan` int(11) NOT NULL,
  `nik` char(10) NOT NULL,
  `instansi` varchar(100) NOT NULL,
  `tingkat_pendidikan` tinyint(4) NOT NULL,
  `jurusan` varchar(50) NOT NULL,
  `tahun_masuk` smallint(6) NOT NULL,
  `tahun_keluar` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_p_pendidikan`
--

INSERT INTO `tb_p_pendidikan` (`id_pendidikan`, `nik`, `instansi`, `tingkat_pendidikan`, `jurusan`, `tahun_masuk`, `tahun_keluar`) VALUES
(3, 'BSP2100001', 'Politeknik Negeri Bandung (POLBAN)', 5, 'Technique Information Engineering', 2007, 2010);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_p_pengalaman_bahasa`
--

CREATE TABLE `tb_p_pengalaman_bahasa` (
  `id_bahasa` int(11) NOT NULL,
  `nik` char(10) NOT NULL,
  `bahasa` varchar(20) NOT NULL,
  `tingkat` enum('Good','Medium','Low') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_p_pengalaman_bahasa`
--

INSERT INTO `tb_p_pengalaman_bahasa` (`id_bahasa`, `nik`, `bahasa`, `tingkat`) VALUES
(1, 'BSP2100001', 'Indonesia', 'Good'),
(2, 'BSP2100001', 'English', 'Medium');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_p_pengalaman_kerja`
--

CREATE TABLE `tb_p_pengalaman_kerja` (
  `id_pengalaman_kerja` int(11) NOT NULL,
  `nik` char(10) NOT NULL,
  `perusahaan` varchar(100) NOT NULL,
  `tanggal_masuk` date NOT NULL,
  `tanggal_keluar` date NOT NULL,
  `status_pegawai` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_p_pengalaman_kerja`
--

INSERT INTO `tb_p_pengalaman_kerja` (`id_pengalaman_kerja`, `nik`, `perusahaan`, `tanggal_masuk`, `tanggal_keluar`, `status_pegawai`) VALUES
(1, 'BSP2100001', 'PT BSP', '2010-10-01', '2021-03-30', 'Full Time Employee'),
(2, 'BSP2100001', 'PT Fujitsu Indonesia', '2010-10-01', '2011-06-30', 'Outsource Employee '),
(4, 'BSP2100001', 'PT Global Dinamika Informatika', '2009-07-01', '2009-09-30', 'Part Time Employee');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_p_pengalaman_kerja_role`
--

CREATE TABLE `tb_p_pengalaman_kerja_role` (
  `id_role` int(11) NOT NULL,
  `id_pengalaman_kerja` int(11) NOT NULL,
  `kd_jabatan` char(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_p_pengalaman_kerja_role`
--

INSERT INTO `tb_p_pengalaman_kerja_role` (`id_role`, `id_pengalaman_kerja`, `kd_jabatan`) VALUES
(1, 1, 'RL001'),
(2, 1, 'RL002'),
(3, 2, 'RL004'),
(5, 4, 'RL004');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_p_prestasi`
--

CREATE TABLE `tb_p_prestasi` (
  `id_prestasi` int(11) NOT NULL,
  `nik` char(10) NOT NULL,
  `kd_jabatan` char(5) NOT NULL,
  `detail` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_p_prestasi`
--

INSERT INTO `tb_p_prestasi` (`id_prestasi`, `nik`, `kd_jabatan`, `detail`) VALUES
(1, 'BSP2100001', 'RL001', 'Involved in manage facilitator and resource to share knowledge about programming'),
(2, 'BSP2100001', 'RL002', 'Involved in facilitate a new programmer to learn about java programming, NodeJs, VueJs, etc.\r\nInvolved in sharing experience to a new programmer.'),
(9, 'BSP2100001', 'RL003', 'Involved in design application, and standardization of coding in order to facilitate the application maintenance.\r\nInvolved in program specification, programming, improvement on programmer productivity.'),
(10, 'BSP2100001', 'RL005', 'Involved in testing the application in order to achieve good performance');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_p_prestasi_detail`
--

CREATE TABLE `tb_p_prestasi_detail` (
  `id_detail_prestasi` int(11) NOT NULL,
  `id_prestasi` int(11) NOT NULL,
  `detail` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_p_prestasi_detail`
--

INSERT INTO `tb_p_prestasi_detail` (`id_detail_prestasi`, `id_prestasi`, `detail`) VALUES
(1, 1, 'Involved in manage facilitator and resource to share knowledge about programming'),
(2, 2, 'Involved in facilitate a new programmer to learn about java programming, NodeJs, VueJs, etc.'),
(6, 2, 'Involved in sharing experience to a new programmer.'),
(7, 9, 'Involved in design application, and standardization of coding in order to facilitate the application maintenance.'),
(8, 9, 'Involved in program specification, programming, improvement on programmer productivity.');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_p_profile`
--

CREATE TABLE `tb_p_profile` (
  `id_profile` int(11) NOT NULL,
  `nik` char(10) NOT NULL,
  `list_profile` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_p_profile`
--

INSERT INTO `tb_p_profile` (`id_profile`, `nik`, `list_profile`) VALUES
(1, 'BSP2100001', '1. Almost 9 year experience as professional IT Programmer, Tester, and Facilitator\r\n2. Goal-oriented individual with good analytic thinking, fast leaner, result oriented');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_p_profile_tech`
--

CREATE TABLE `tb_p_profile_tech` (
  `id_ptecg` int(11) NOT NULL,
  `nik` char(10) NOT NULL,
  `list_ptech` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_p_profile_tech`
--

INSERT INTO `tb_p_profile_tech` (`id_ptecg`, `nik`, `list_ptech`) VALUES
(1, 'BSP2100001', '1. Programming Language: Java (Android),  Java (J2EE), SQL, Jsp/Servlet, C#, Python, Ruby, JavaScript\r\n2.Framework : Struts, Spring, Jasper Reports, POI, ASP.NET, ASP.NET MVC 3, ASP.NET MVC 4, Web API, Terasoluna, MyBatis, Hibernet, VueJS, NodeJS');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_user`
--

CREATE TABLE `tb_user` (
  `id_user` int(11) NOT NULL,
  `nik` char(10) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_user`
--

INSERT INTO `tb_user` (`id_user`, `nik`, `password`) VALUES
(5, 'BSP2100003', '$2a$10$rig2DNWHWR0vMFeJZwqFnu1R8EA2t3SK9SHXA4EJCbQu9Vm5ZUBxC'),
(7, 'BSP2100001', '$2a$10$52cecw2xb32yNdS6tAJJyO31LE0QC2nPqEEL3Fu2ZMqGswOpa40RS'),
(9, 'BSP2100005', '$2a$10$p0MZOJIxatGY/07ieFbct.4YF7V5y9LV57MZLGWEONq5Gl7nWcE0S');

--
-- Trigger `tb_user`
--
DELIMITER $$
CREATE TRIGGER `after_user_insert` AFTER INSERT ON `tb_user` FOR EACH ROW BEGIN
	INSERT INTO tb_personal(nik)
	VALUES(new.nik);
END
$$
DELIMITER ;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `tb_m_agama`
--
ALTER TABLE `tb_m_agama`
  ADD PRIMARY KEY (`kd_agama`);

--
-- Indeks untuk tabel `tb_m_daftar_pekerjaan`
--
ALTER TABLE `tb_m_daftar_pekerjaan`
  ADD PRIMARY KEY (`kd_jabatan`);

--
-- Indeks untuk tabel `tb_m_gelar`
--
ALTER TABLE `tb_m_gelar`
  ADD PRIMARY KEY (`kd_gelar`);

--
-- Indeks untuk tabel `tb_personal`
--
ALTER TABLE `tb_personal`
  ADD PRIMARY KEY (`nik`),
  ADD KEY `agama` (`agama`),
  ADD KEY `role_terakhir` (`role_terakhir`);

--
-- Indeks untuk tabel `tb_project`
--
ALTER TABLE `tb_project`
  ADD PRIMARY KEY (`id_project`),
  ADD KEY `nik` (`nik`);

--
-- Indeks untuk tabel `tb_project_app`
--
ALTER TABLE `tb_project_app`
  ADD PRIMARY KEY (`id_app`),
  ADD KEY `id_project` (`id_project`);

--
-- Indeks untuk tabel `tb_project_db`
--
ALTER TABLE `tb_project_db`
  ADD PRIMARY KEY (`id_db`),
  ADD KEY `id_project` (`id_project`);

--
-- Indeks untuk tabel `tb_project_framework`
--
ALTER TABLE `tb_project_framework`
  ADD PRIMARY KEY (`id_framework`),
  ADD KEY `id_project` (`id_project`);

--
-- Indeks untuk tabel `tb_project_lang`
--
ALTER TABLE `tb_project_lang`
  ADD PRIMARY KEY (`id_lang`),
  ADD KEY `id_project` (`id_project`);

--
-- Indeks untuk tabel `tb_project_os`
--
ALTER TABLE `tb_project_os`
  ADD PRIMARY KEY (`id_os`),
  ADD KEY `id_project` (`id_project`);

--
-- Indeks untuk tabel `tb_project_role`
--
ALTER TABLE `tb_project_role`
  ADD PRIMARY KEY (`id_pr`),
  ADD KEY `role` (`role`),
  ADD KEY `fk_project` (`id_project`);

--
-- Indeks untuk tabel `tb_project_server`
--
ALTER TABLE `tb_project_server`
  ADD PRIMARY KEY (`id_server`),
  ADD KEY `id_project` (`id_project`);

--
-- Indeks untuk tabel `tb_project_tool`
--
ALTER TABLE `tb_project_tool`
  ADD PRIMARY KEY (`id_tool`),
  ADD KEY `id_project` (`id_project`);

--
-- Indeks untuk tabel `tb_p_kursus`
--
ALTER TABLE `tb_p_kursus`
  ADD PRIMARY KEY (`id_kursus`),
  ADD KEY `nik` (`nik`),
  ADD KEY `tb_p_kursus_role` (`role`);

--
-- Indeks untuk tabel `tb_p_pendidikan`
--
ALTER TABLE `tb_p_pendidikan`
  ADD PRIMARY KEY (`id_pendidikan`),
  ADD KEY `nik` (`nik`),
  ADD KEY `tingkat_pendidikan` (`tingkat_pendidikan`);

--
-- Indeks untuk tabel `tb_p_pengalaman_bahasa`
--
ALTER TABLE `tb_p_pengalaman_bahasa`
  ADD PRIMARY KEY (`id_bahasa`),
  ADD KEY `nik` (`nik`);

--
-- Indeks untuk tabel `tb_p_pengalaman_kerja`
--
ALTER TABLE `tb_p_pengalaman_kerja`
  ADD PRIMARY KEY (`id_pengalaman_kerja`),
  ADD KEY `nik` (`nik`);

--
-- Indeks untuk tabel `tb_p_pengalaman_kerja_role`
--
ALTER TABLE `tb_p_pengalaman_kerja_role`
  ADD PRIMARY KEY (`id_role`),
  ADD KEY `kd_jabatan` (`kd_jabatan`),
  ADD KEY `tb_p_pengalaman_kerja_role_ibfk_1` (`id_pengalaman_kerja`);

--
-- Indeks untuk tabel `tb_p_prestasi`
--
ALTER TABLE `tb_p_prestasi`
  ADD PRIMARY KEY (`id_prestasi`),
  ADD KEY `nik` (`nik`),
  ADD KEY `kd_jabatan` (`kd_jabatan`);

--
-- Indeks untuk tabel `tb_p_prestasi_detail`
--
ALTER TABLE `tb_p_prestasi_detail`
  ADD PRIMARY KEY (`id_detail_prestasi`),
  ADD KEY `id_prestasi` (`id_prestasi`);

--
-- Indeks untuk tabel `tb_p_profile`
--
ALTER TABLE `tb_p_profile`
  ADD PRIMARY KEY (`id_profile`),
  ADD KEY `nik` (`nik`);

--
-- Indeks untuk tabel `tb_p_profile_tech`
--
ALTER TABLE `tb_p_profile_tech`
  ADD PRIMARY KEY (`id_ptecg`),
  ADD KEY `nik` (`nik`);

--
-- Indeks untuk tabel `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `tb_m_agama`
--
ALTER TABLE `tb_m_agama`
  MODIFY `kd_agama` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `tb_m_gelar`
--
ALTER TABLE `tb_m_gelar`
  MODIFY `kd_gelar` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `tb_project`
--
ALTER TABLE `tb_project`
  MODIFY `id_project` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `tb_project_app`
--
ALTER TABLE `tb_project_app`
  MODIFY `id_app` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tb_project_db`
--
ALTER TABLE `tb_project_db`
  MODIFY `id_db` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tb_project_framework`
--
ALTER TABLE `tb_project_framework`
  MODIFY `id_framework` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `tb_project_lang`
--
ALTER TABLE `tb_project_lang`
  MODIFY `id_lang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `tb_project_os`
--
ALTER TABLE `tb_project_os`
  MODIFY `id_os` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tb_project_role`
--
ALTER TABLE `tb_project_role`
  MODIFY `id_pr` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `tb_project_server`
--
ALTER TABLE `tb_project_server`
  MODIFY `id_server` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `tb_project_tool`
--
ALTER TABLE `tb_project_tool`
  MODIFY `id_tool` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tb_p_kursus`
--
ALTER TABLE `tb_p_kursus`
  MODIFY `id_kursus` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT untuk tabel `tb_p_pendidikan`
--
ALTER TABLE `tb_p_pendidikan`
  MODIFY `id_pendidikan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `tb_p_pengalaman_bahasa`
--
ALTER TABLE `tb_p_pengalaman_bahasa`
  MODIFY `id_bahasa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `tb_p_pengalaman_kerja`
--
ALTER TABLE `tb_p_pengalaman_kerja`
  MODIFY `id_pengalaman_kerja` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `tb_p_pengalaman_kerja_role`
--
ALTER TABLE `tb_p_pengalaman_kerja_role`
  MODIFY `id_role` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `tb_p_prestasi`
--
ALTER TABLE `tb_p_prestasi`
  MODIFY `id_prestasi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT untuk tabel `tb_p_prestasi_detail`
--
ALTER TABLE `tb_p_prestasi_detail`
  MODIFY `id_detail_prestasi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `tb_p_profile`
--
ALTER TABLE `tb_p_profile`
  MODIFY `id_profile` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `tb_p_profile_tech`
--
ALTER TABLE `tb_p_profile_tech`
  MODIFY `id_ptecg` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `tb_personal`
--
ALTER TABLE `tb_personal`
  ADD CONSTRAINT `tb_personal_ibfk_1` FOREIGN KEY (`agama`) REFERENCES `tb_m_agama` (`kd_agama`),
  ADD CONSTRAINT `tb_personal_ibfk_2` FOREIGN KEY (`role_terakhir`) REFERENCES `tb_m_daftar_pekerjaan` (`kd_jabatan`);

--
-- Ketidakleluasaan untuk tabel `tb_project`
--
ALTER TABLE `tb_project`
  ADD CONSTRAINT `tb_project_ibfk_1` FOREIGN KEY (`nik`) REFERENCES `tb_personal` (`nik`);

--
-- Ketidakleluasaan untuk tabel `tb_project_app`
--
ALTER TABLE `tb_project_app`
  ADD CONSTRAINT `tb_project_app_ibfk_1` FOREIGN KEY (`id_project`) REFERENCES `tb_project` (`id_project`);

--
-- Ketidakleluasaan untuk tabel `tb_project_db`
--
ALTER TABLE `tb_project_db`
  ADD CONSTRAINT `tb_project_db_ibfk_1` FOREIGN KEY (`id_project`) REFERENCES `tb_project` (`id_project`);

--
-- Ketidakleluasaan untuk tabel `tb_project_framework`
--
ALTER TABLE `tb_project_framework`
  ADD CONSTRAINT `tb_project_framework_ibfk_1` FOREIGN KEY (`id_project`) REFERENCES `tb_project` (`id_project`);

--
-- Ketidakleluasaan untuk tabel `tb_project_lang`
--
ALTER TABLE `tb_project_lang`
  ADD CONSTRAINT `tb_project_lang_ibfk_1` FOREIGN KEY (`id_project`) REFERENCES `tb_project` (`id_project`);

--
-- Ketidakleluasaan untuk tabel `tb_project_os`
--
ALTER TABLE `tb_project_os`
  ADD CONSTRAINT `tb_project_os_ibfk_1` FOREIGN KEY (`id_project`) REFERENCES `tb_project` (`id_project`);

--
-- Ketidakleluasaan untuk tabel `tb_project_role`
--
ALTER TABLE `tb_project_role`
  ADD CONSTRAINT `fk_project` FOREIGN KEY (`id_project`) REFERENCES `tb_project` (`id_project`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_project_role_ibfk_2` FOREIGN KEY (`role`) REFERENCES `tb_m_daftar_pekerjaan` (`kd_jabatan`);

--
-- Ketidakleluasaan untuk tabel `tb_project_server`
--
ALTER TABLE `tb_project_server`
  ADD CONSTRAINT `tb_project_server_ibfk_1` FOREIGN KEY (`id_project`) REFERENCES `tb_project` (`id_project`);

--
-- Ketidakleluasaan untuk tabel `tb_project_tool`
--
ALTER TABLE `tb_project_tool`
  ADD CONSTRAINT `tb_project_tool_ibfk_1` FOREIGN KEY (`id_project`) REFERENCES `tb_project` (`id_project`);

--
-- Ketidakleluasaan untuk tabel `tb_p_kursus`
--
ALTER TABLE `tb_p_kursus`
  ADD CONSTRAINT `tb_p_kursus_ibfk_1` FOREIGN KEY (`nik`) REFERENCES `tb_personal` (`nik`),
  ADD CONSTRAINT `tb_p_kursus_role` FOREIGN KEY (`role`) REFERENCES `tb_m_daftar_pekerjaan` (`kd_jabatan`);

--
-- Ketidakleluasaan untuk tabel `tb_p_pendidikan`
--
ALTER TABLE `tb_p_pendidikan`
  ADD CONSTRAINT `tb_p_pendidikan_ibfk_1` FOREIGN KEY (`nik`) REFERENCES `tb_personal` (`nik`),
  ADD CONSTRAINT `tb_p_pendidikan_ibfk_2` FOREIGN KEY (`tingkat_pendidikan`) REFERENCES `tb_m_gelar` (`kd_gelar`);

--
-- Ketidakleluasaan untuk tabel `tb_p_pengalaman_bahasa`
--
ALTER TABLE `tb_p_pengalaman_bahasa`
  ADD CONSTRAINT `tb_p_pengalaman_bahasa_ibfk_1` FOREIGN KEY (`nik`) REFERENCES `tb_personal` (`nik`);

--
-- Ketidakleluasaan untuk tabel `tb_p_pengalaman_kerja`
--
ALTER TABLE `tb_p_pengalaman_kerja`
  ADD CONSTRAINT `tb_p_pengalaman_kerja_ibfk_1` FOREIGN KEY (`nik`) REFERENCES `tb_personal` (`nik`);

--
-- Ketidakleluasaan untuk tabel `tb_p_pengalaman_kerja_role`
--
ALTER TABLE `tb_p_pengalaman_kerja_role`
  ADD CONSTRAINT `tb_p_pengalaman_kerja_role_ibfk_1` FOREIGN KEY (`id_pengalaman_kerja`) REFERENCES `tb_p_pengalaman_kerja` (`id_pengalaman_kerja`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_p_pengalaman_kerja_role_ibfk_2` FOREIGN KEY (`kd_jabatan`) REFERENCES `tb_m_daftar_pekerjaan` (`kd_jabatan`);

--
-- Ketidakleluasaan untuk tabel `tb_p_prestasi`
--
ALTER TABLE `tb_p_prestasi`
  ADD CONSTRAINT `tb_p_prestasi_ibfk_1` FOREIGN KEY (`nik`) REFERENCES `tb_personal` (`nik`),
  ADD CONSTRAINT `tb_p_prestasi_ibfk_2` FOREIGN KEY (`kd_jabatan`) REFERENCES `tb_m_daftar_pekerjaan` (`kd_jabatan`);

--
-- Ketidakleluasaan untuk tabel `tb_p_prestasi_detail`
--
ALTER TABLE `tb_p_prestasi_detail`
  ADD CONSTRAINT `tb_p_prestasi_detail_ibfk_1` FOREIGN KEY (`id_prestasi`) REFERENCES `tb_p_prestasi` (`id_prestasi`);

--
-- Ketidakleluasaan untuk tabel `tb_p_profile`
--
ALTER TABLE `tb_p_profile`
  ADD CONSTRAINT `tb_p_profile_ibfk_1` FOREIGN KEY (`nik`) REFERENCES `tb_personal` (`nik`);

--
-- Ketidakleluasaan untuk tabel `tb_p_profile_tech`
--
ALTER TABLE `tb_p_profile_tech`
  ADD CONSTRAINT `tb_p_profile_tech_ibfk_1` FOREIGN KEY (`nik`) REFERENCES `tb_personal` (`nik`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
